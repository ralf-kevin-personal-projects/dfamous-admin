<?php include '../headers/dashboard-header.php'; ?>

      
    <main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
        <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
            <h1 class="h2">Applicants</h1>
            <div class="btn-toolbar mb-2 mb-md-0">
            <div class="btn-group mr-2">
                <!-- <button class="btn btn-sm btn-outline-secondary">Share</button>
                <button class="btn btn-sm btn-outline-secondary">Export</button> -->
                <!-- <button class="btn btn-md btn-outline-secondary">Add New Applicant</button> -->
                
            </div>
            <!-- <button class="btn btn-sm btn-outline-secondary dropdown-toggle">
                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-calendar"><rect x="3" y="4" width="18" height="18" rx="2" ry="2"></rect><line x1="16" y1="2" x2="16" y2="6"></line><line x1="8" y1="2" x2="8" y2="6"></line><line x1="3" y1="10" x2="21" y2="10"></line></svg>
                This week
            </button> -->
            </div>
        </div>
        
        <div class="">

                <div class="table-responsive">
                    <table id="tbl-applicant" class="table table-striped table-sm">
                    <thead>
                        <tr>
                        <th>Name</th>
                        <th>Contact</th>
                        <th>Applied Company</th>
                        <th>CV/Resume</th>
                        <th>Status</th>
                        <th>Action</th>
                        <th>Override</th>
                        </tr>
                    </thead>
                    <tbody>                             
                    </tbody>
                    </table>
                </div>
        </div>


    </main>


<!-- Modal -->
<div class="modal fade" id="appModal" tabindex="-1" role="dialog" aria-labelledby="appModalTitle" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="appModalTitle">Add New Company Client</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close" onclick=location.reload()>
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
          

            <div class="form-group">
                
                <h6>Personal Information</h6>
                <div class="row">
                        <div class="col-md-4">
                            <label>Given Name</label>                                                                
                            <div class="form-group">
                                <input id="given_name" type="text" class="form-control" placeholder="Given name"/>
                            </div>
                        </div>                    
                        <div class="col-md-4">
                            <label>Sur Name</label>                                                                                        
                            <div class="form-group">                        
                                <input id="sur_name" type="text" class="form-control" placeholder="Sur name"/>
                            </div>
                        </div>                    
                        <div class="col-md-4">
                            <label>Middle Name</label>                                                                                        
                            <div class="form-group">                                                
                                <input id="middle_name" type="text" class="form-control" placeholder="Middle name"/>
                            </div>
                        </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">                                                                    
                            <label>Home Address</label>                        
                            <input id="address" type="text" class="form-control" placeholder="Home Address"/>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">       
                            <label>Mobile number 1</label>                                                                  
                            <input id="number" type="number" class="form-control" placeholder="Mobile Number"/>
                        </div>                       
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">       
                            <label>Mobile number 2</label>                                                                  
                            <input id="number2" type="number" class="form-control" placeholder="Mobile Number"/>
                        </div>                       
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Date of Birth</label>                                                                  
                            <input id="bday" type="date" class="form-control" placeholder="Date of Birth"/>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <label>Place of Birth</label>                                      
                            <input id="bdate" type="number" class="form-control" placeholder="Place of Birth"/>
                        </div>                       
                    </div>  
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Gender</label>                                                                  
                            <select id="gender" class="form-control">
                                <option value="">Select Gender</option>
                                <option value="">Male</option>                            
                                <option value="">Female</option>                                                        
                            </select>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Citizenship</label>                                      
                            <input id="citizenship" type="text" class="form-control" placeholder="Citizenship"/>
                        </div>                       
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Civil Status</label>
                            <select id="civil" class="form-control">
                                <option value="">Select Civil Status</option>
                                <option value="">Single</option>                            
                                <option value="">Married</option>                                                        
                                <option value="">Widowed</option>
                            </select>
                        </div>
                    </div>                
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Height</label>                                                                  
                            <input id="height" type="text" class="form-control" placeholder="Height"/>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Weight</label>                                      
                            <input id="weight" type="text" class="form-control" placeholder="Weight"/>
                        </div>                       
                    </div>             
                </div>

            </div>
            <hr/>

            <div class="form-group">
            
                <h6>* Skip this if you select civil status as single</h6>
                <div class="row">
                        <div class="col-md-8">
                            <label>Name of Spouse</label>                                                                
                            <div class="form-group">
                                <input id="spouse" type="text" class="form-control" placeholder="Name of Spouse"/>
                            </div>
                        </div>                    
                        <div class="col-md-4">
                            <label>Occupation</label>                                                                                        
                            <div class="form-group">                        
                                <input id="spouse_work" type="text" class="form-control" placeholder="Occupation"/>
                            </div>
                        </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">       
                            <label>Mobile number 1</label>                                                                  
                            <input id="spouse_num" type="number" class="form-control" placeholder="Mobile Number"/>
                        </div>                       
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">       
                            <label>Mobile number 2</label>                                                                  
                            <input id="spouse_num2" type="number" class="form-control" placeholder="Mobile Number"/>
                        </div>                       
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-8">
                        <label>Name of Children</label>                                                                
                        <div class="form-group">
                            <input id="child_name" type="text" class="form-control" placeholder="Name of Children"/>
                        </div>
                    </div>                    
                    <div class="col-md-4">
                        <label>Age</label>                                                                                        
                        <div class="form-group">                        
                            <input id="child_age" type="number" class="form-control" placeholder="Age"/>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-8">
                        <label>Name of Children</label>                                                                
                        <div class="form-group">
                            <input id="child_name2" type="text" class="form-control" placeholder="Name of Children"/>
                        </div>
                    </div>                    
                    <div class="col-md-4">
                        <label>Age</label>                                                                                        
                        <div class="form-group">                        
                            <input id="child_age2" type="number" class="form-control" placeholder="Age"/>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-8">
                        <label>Name of Children</label>                                                                
                        <div class="form-group">
                            <input id="child_name3" type="text" class="form-control" placeholder="Name of Children"/>
                        </div>
                    </div>                    
                    <div class="col-md-4">
                        <label>Age</label>                                                                                        
                        <div class="form-group">                        
                            <input id="child_age3" type="number" class="form-control" placeholder="Age"/>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-8">
                        <label>Name of Children</label>                                                                
                        <div class="form-group">
                            <input id="child_name4" type="text" class="form-control" placeholder="Name of Children"/>
                        </div>
                    </div>                    
                    <div class="col-md-4">
                        <label>Age</label>                                                                                        
                        <div class="form-group">                        
                            <input id="child_age4" type="number" class="form-control" placeholder="Age"/>
                        </div>
                    </div>
                </div>                                                


            </div>            
            <hr/>


            <div class="form-group">
            
                <h6>Parent Information</h6>
                <div class="row">
                        <div class="col-md-6">
                            <label>Name of Father</label>                                                                
                            <div class="form-group">
                                <input id="father_name" type="text" class="form-control" placeholder="Name of Father"/>
                            </div>
                        </div>                    
                        <div class="col-md-3">
                            <label>Occupation</label>                                                                                        
                            <div class="form-group">                        
                                <input id="father_work" type="text" class="form-control" placeholder="Occupation"/>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">       
                                <label>Mobile number</label>                                                                  
                                <input id="father_num" type="number" class="form-control" placeholder="Mobile Number"/>
                            </div>                       
                        </div>                    
                </div>
                <div class="row">
                        <div class="col-md-6">
                            <label>Name of Mother</label>                                                                
                            <div class="form-group">
                                <input id="mother_name" type="text" class="form-control" placeholder="Name of Mother"/>
                            </div>
                        </div>                    
                        <div class="col-md-3">
                            <label>Occupation</label>                                                                                        
                            <div class="form-group">                        
                                <input id="mother_work" type="text" class="form-control" placeholder="Occupation"/>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">       
                                <label>Mobile number</label>                                                                  
                                <input id="mother_num" type="number" class="form-control" placeholder="Mobile Number"/>
                            </div>                       
                        </div>                    
                </div>            
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">       
                            <label>Address</label>                                                                  
                            <input id="parent_add" type="text" class="form-control" placeholder="Address"/>
                        </div>                       
                    </div>
                </div>
            </div>
            <hr/>

            <div class="form-group">
            
                <h6>For Emergency</h6>
                <div class="row">
                        <div class="col-md-12">
                            <label>Person to be notified in case of Emergency</label>                                                                
                            <div class="form-group">
                                <input id="emergency_name" type="text" class="form-control" placeholder="Person to be notified in case of Emergency"/>
                            </div>
                        </div>                    
                </div>
                <div class="row">
                        <div class="col-md-8">
                            <label>Address</label>                                                                
                            <div class="form-group">
                                <input id="emergency_add" type="text" class="form-control" placeholder="Address"/>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">       
                                <label>Mobile number</label>                                                                  
                                <input id="emergency_num" type="number" class="form-control" placeholder="Mobile Number"/>
                            </div>                       
                        </div>                    
                </div>            


            </div>
            <hr/>

            <div class="form-group">
                
                <h6>Educational Background</h6>
                <div class="row">
                        <div class="col-md-9">
                            <label>Name of School (Primary)</label>                                            
                            <div class="form-group">
                                <input id="schl_pri" type="text" class="form-control" placeholder="Name of School (Primary)"/>
                            </div>
                        </div>                    
                        <div class="col-md-3">
                            <label>Year Attended</label>                                                                                        
                            <div class="form-group">                        
                                <input id="schl_pri_yr" type="text" class="form-control" placeholder="Year Attended"/>
                            </div>
                        </div>
                </div>
                <div class="row">
                        <div class="col-md-9">
                            <label>Name of School (High School)</label>                                            
                            <div class="form-group">
                                <input id="schl_hs" type="text" class="form-control" placeholder="Name of School (High School)"/>
                            </div>
                        </div>                    
                        <div class="col-md-3">
                            <label>Year Attended</label>                                                                                        
                            <div class="form-group">                        
                                <input id="schl_hs_yr" type="text" class="form-control" placeholder="Year Attended"/>
                            </div>
                        </div>                    
                </div>            
                <div class="row">
                        <div class="col-md-7">
                            <label>Name of School (College)</label>                                            
                            <div class="form-group">
                                <input id="schl_college" type="text" class="form-control" placeholder="Name of School (College)"/>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <label>Course</label>                                            
                            <div class="form-group">
                                <input id="schl_college_course" type="text" class="form-control" placeholder="Course"/>
                            </div>
                        </div>                                         
                        <div class="col-md-3">
                            <label>Year Attended</label>                                                                                        
                            <div class="form-group">                        
                                <input id="schl_college_yr" type="text" class="form-control" placeholder="Year Attended"/>
                            </div>
                        </div>
                </div>


            </div>
            <hr/>

            <div class="form-group">
                
                <h6>Employment History</h6>
                <div class="row">
                        <div class="col-md-5">
                            <label>Name of Company</label>                                            
                            <div class="form-group">
                                <input id="company" type="text" class="form-control" placeholder="Name of Company"/>
                            </div>
                        </div>                    
                        <div class="col-md-3">
                            <label>Position</label>                                                                                        
                            <div class="form-group">                        
                                <input id="company_pos" type="text" class="form-control" placeholder="Position"/>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <label>From</label>                                                                                        
                            <div class="form-group">                        
                                <input id="company_frm" type="text" class="form-control" placeholder="From"/>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <label>To</label>                                                                                        
                            <div class="form-group">                        
                                <input id="company_to" type="text" class="form-control" placeholder="To"/>
                            </div>
                        </div>                                                
                </div>
                <div class="row">
                        <div class="col-md-5">
                            <label>Name of Company</label>                                            
                            <div class="form-group">
                                <input id="company2" type="text" class="form-control" placeholder="Name of Company"/>
                            </div>
                        </div>                    
                        <div class="col-md-3">
                            <label>Position</label>                                                                                        
                            <div class="form-group">                        
                                <input id="company2_pos" type="text" class="form-control" placeholder="Position"/>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <label>From</label>                                                                                        
                            <div class="form-group">                        
                                <input id="company2_frm" type="text" class="form-control" placeholder="From"/>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <label>To</label>                                                                                        
                            <div class="form-group">                        
                                <input id="company2_to" type="text" class="form-control" placeholder="To"/>
                            </div>
                        </div>                                                
                </div>
                <div class="row">
                        <div class="col-md-5">
                            <label>Name of Company</label>                                            
                            <div class="form-group">
                                <input id="company3" type="text" class="form-control" placeholder="Name of Company"/>
                            </div>
                        </div>                    
                        <div class="col-md-3">
                            <label>Position</label>                                                                                        
                            <div class="form-group">                        
                                <input id="company3_pos" type="text" class="form-control" placeholder="Position"/>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <label>From</label>                                                                                        
                            <div class="form-group">                        
                                <input id="company3_frm" type="text" class="form-control" placeholder="From"/>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <label>To</label>                                                                                        
                            <div class="form-group">                        
                                <input id="company3_to" type="text" class="form-control" placeholder="To"/>
                            </div>
                        </div>                                                
                </div>
                
            </div>
            <hr/>

            <div class="form-group">
                
                <h6></h6>
                <div class="row">
                        <div class="col-md-6">
                            <label>SSS #</label>                                            
                            <div class="form-group">
                                <input id="sss" type="text" class="form-control" placeholder="SSS #"/>
                            </div>
                        </div>                    
                        <div class="col-md-6">
                            <label>Phil Health #</label>                                                                                        
                            <div class="form-group">                        
                                <input id="philhealth" type="text" class="form-control" placeholder="Phil Health #"/>
                            </div>
                        </div>                                              
                </div>
                <div class="row">
                        <div class="col-md-6">
                            <label>Pag Ibig #</label>                                            
                            <div class="form-group">
                                <input id="pagibig" type="text" class="form-control" placeholder="Pag Ibig #"/>
                            </div>
                        </div>                    
                        <div class="col-md-6">
                            <label>TIN #</label>                                                                                        
                            <div class="form-group">                        
                                <input id="tin" type="text" class="form-control" placeholder="TIN #"/>
                            </div>
                        </div>                                              
                </div>


            </div>


            
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal" onclick=location.reload()>Close</button>
        <button id="btnSave" type="button" class="btn btn-success">Create Company Account</button>
      </div>
    </div>
  </div>
</div>


<?php include '../headers/dashboard-footer.php'; ?>



<script>
    $(document).ready(function(){

        var pdf_root = "../../dfamous-job/uploads/";

        loadData();


        $(document).on("click", "#btn-view", function() {
            $("#appModal").modal("show");
        });

        $(document).on("change", "#status", function() {
            
            var status = $(this).val();
            var app_id = $(this).find("option:selected").data("id");

            if(confirm("Are you sure you want to override the status of this applicant?")){
                overrideStatus(app_id, status)
            }
        });
        
        function overrideStatus(id, status) {
            var fd = new FormData();

            fd.append("app_id", id);
            fd.append("app_status", status);
            fd.append("request", "app_override");

            $.ajax({
                type: "POST",
                url: "../classes/Requests.php",
                data: fd,
                contentType: false,
                cache: false,
                processData: false,                  
                dataType: "json",
                success: function(res){
                    alert("response " + res.result)
                    location.reload();
                    console.log(res);
                }, error: function() {
                    alert("error handler")
                }
            });
        }

        function loadData() {

            var fd = new FormData();            
            fd.append("request", "fetch_applicant");

            $.ajax({
                type: "POST",
                url: "../classes/Requests.php",
                data: fd,
                contentType: false,
                cache: false,
                processData: false,                  
                dataType: "json",
                success: function(res){
                    // alert("response " + res.result[2]["post_title"])
                    if (res.success == true) {
                        populateData(res.result);
                    } else {
                        alert(res.result);
                    }

                    console.log(res);
                }, error: function() {
                    alert("error handler")
                }
            });
        }

        function populateData(datas) {

            var tmpl;

            if (datas.length > 0) {

                for (var i = 0; i < datas.length; i++) {

                    var app_id = datas[i]["app_id"];
                    var app_fname = datas[i]["app_fname"];
                    var app_mname = datas[i]["app_mname"];
                    var app_lname = datas[i]["app_lname"];
                    var app_address = datas[i]["app_address"];
                    var app_mobile1 = datas[i]["app_mobile1"];
                    var app_mobile2 = datas[i]["app_mobile2"];
                    var app_bday = datas[i]["app_bday"];
                    var app_bplace = datas[i]["app_bplace"];
                    var app_gender = datas[i]["app_gender"];
                    var app_citizenship = datas[i]["app_citizenship"];
                    var app_civil = datas[i]["app_civil"];
                    var app_height = datas[i]["app_height"];
                    var app_weight = datas[i]["app_weight"];
                    var app_spouse = datas[i]["app_spouse"];
                    var app_spouse_occupation = datas[i]["app_spouse_occupation"];
                    var app_spouse_mobile1 = datas[i]["app_spouse_mobile1"];
                    var app_spouse_mobile2 = datas[i]["app_spouse_mobile2"];
                    var app_children1_name = datas[i]["app_children1_name"];
                    var app_children1_age = datas[i]["app_children1_age"];
                    var app_children2_name = datas[i]["app_children2_name"];
                    var app_children2_age = datas[i]["app_children2_age"];
                    var app_children3_name = datas[i]["app_children3_name"];
                    var app_children3_age = datas[i]["app_children3_age"];
                    var app_children4_name = datas[i]["app_children4_name"];
                    var app_children4_age = datas[i]["app_children4_age"];
                    var app_father = datas[i]["app_father"];
                    var app_father_occupation = datas[i]["app_father_occupation"];
                    var app_father_mobile = datas[i]["app_father_mobile"];
                    var app_mother = datas[i]["app_mother"];
                    var app_mother_occupation = datas[i]["app_mother_occupation"];
                    var app_mother_mobile = datas[i]["app_mother_mobile"];
                    var app_parent_add = datas[i]["app_parent_add"];
                    var app_emergency_person = datas[i]["app_emergency_person"];
                    var app_emergency_add = datas[i]["app_emergency_add"];
                    var app_emergency_mobile = datas[i]["app_emergency_mobile"];
                    var app_schl_pri = datas[i]["app_schl_pri"];
                    var app_schl_pri_yr = datas[i]["app_schl_pri_yr"];
                    var app_schl_hs = datas[i]["app_schl_hs"];
                    var app_schl_hs_yr = datas[i]["app_schl_hs_yr"];
                    var app_schl_college = datas[i]["app_schl_college"];
                    var app_schl_course = datas[i]["app_schl_course"];
                    var app_schl_course_yr = datas[i]["app_schl_course_yr"];
                    var app_cmpny1 = datas[i]["app_cmpny1"];
                    var app_cmpny1_pos = datas[i]["app_cmpny1_pos"];
                    var app_cmpny1_from = datas[i]["app_cmpny1_from"];
                    var app_cmpny1_to = datas[i]["app_cmpny1_to"];
                    var app_cmpny2 = datas[i]["app_cmpny2"];
                    var app_cmpny2_pos = datas[i]["app_cmpny2_pos"];
                    var app_cmpny2_from = datas[i]["app_cmpny2_from"];
                    var app_cmpny2_to = datas[i]["app_cmpny2_to"];
                    var app_cmpny3 = datas[i]["app_cmpny3"];
                    var app_cmpny3_pos = datas[i]["app_cmpny3_pos"];
                    var app_cmpny3_from = datas[i]["app_cmpny3_from"];
                    var app_cmpny3_to = datas[i]["app_cmpny3_to"];
                    var app_sss = datas[i]["app_sss"];
                    var app_pagibig = datas[i]["app_pagibig"];
                    var app_philhealth = datas[i]["app_philhealth"];
                    var app_tin = datas[i]["app_tin"];

                    var app_cv = datas[i]["app_cv"];

                    var app_status = datas[i]["app_status"];
                    var company_id = datas[i]["company_id"];
                    var company_name = datas[i]["company_name"];

                    var full_name = app_fname + " " + app_lname;

                    tmpl += "<tr>"+
                            "<td>"+ full_name +"</td>"+
                            "<td>"+ app_mobile1  +"</td>"+
                            "<td>"+ company_name +"</td>"+
                            "<td> <a href='"+ pdf_root + app_cv +"' target=_blank>"+ app_cv +"</a></td>"+
                            "<td>"+ app_status +"</td>"+
                            "<td>"+
                                "<div class='form-group'>"+
                                    "<button id='btn-view' class='btn btn-md btn-primary' >"+
                                        // "data-id='"+ id +"' "+
                                        // "data-name='"+ name +"' "+
                                        // "data-address='"+ address +"' "+
                                        // "data-loc='"+ loc +"' "+
                                        // "data-desc='"+ desc +"' "+
                                        // "data-email='"+ email +"' "+
                                        // "data-num='"+ num +"' "+
                                        // "data-user='"+ user +"' "+                                        
                                        // "data-status='"+ status +"' "+
                                        // "data-date='"+ date +"' >"+
                                        "<i class='fas fa-eye'></i>"+
                                    "</button> "+
                                "</div>"+
                            "</td>"+
                            "<td>"+
                                "<div class='form-group col-md-8'>"+
                                    "<select id='status' class='form-control'>"+
                                        "<option value=''>Select Action</option>"+
                                        "<option value='For Interview' data-id='"+ app_id +"'>For Interview</option>"+
                                        "<option value='Void Application' data-id='"+ app_id +"'>Void Application</option>"+
                                        "<option value='Accepted' data-id='"+ app_id +"'>Accepted</option>"+
                                    "</select>"+
                                "</div>"+
                                // "<input id='app_id' type='text' value='"+ app_id +"' />"+
                            "</td>"+
                            "</tr>";

                }
                
            } else {
                Alert("No Result");
            }

            $("#tbl-applicant").find("tbody tr").remove().end()
            $("#tbl-applicant").append(tmpl);
            
        }


        function validateItems(values, keys) {

            var isNotEmpty = false;

            for (var i = 0; i < values.length; i++) {

                if (values[i] == "" || values[i] == null) {
                    $(keys[i]).addClass("border-danger");
                    isNotEmpty = false;
                } else {
                    isNotEmpty = true;
                }
            }

            return isNotEmpty;
        }

    });
</script>