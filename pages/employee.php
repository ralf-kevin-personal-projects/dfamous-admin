<?php include '../headers/dashboard-header.php'; ?>

      
        <main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
        <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
            <h1 class="h2">Employee</h1>
            <div class="btn-toolbar mb-2 mb-md-0">
            <div class="btn-group mr-2">
                <!-- <button class="btn btn-sm btn-outline-secondary">Share</button>
                <button class="btn btn-sm btn-outline-secondary">Export</button> -->
                <button class="btn btn-md btn-outline-secondary" data-toggle="modal" data-target="#empModal">Add New Employee</button>
                
            </div>
            <!-- <button class="btn btn-sm btn-outline-secondary dropdown-toggle">
                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-calendar"><rect x="3" y="4" width="18" height="18" rx="2" ry="2"></rect><line x1="16" y1="2" x2="16" y2="6"></line><line x1="8" y1="2" x2="8" y2="6"></line><line x1="3" y1="10" x2="21" y2="10"></line></svg>
                This week
            </button> -->
            </div>
        </div>
        
        <div class="">

                <div class="table-responsive">
                    <table id="tbl-emp" class="table table-striped table-sm">
                    <thead>
                        <tr>
                        <th>Full name</th>
                        <th>Employment Status</th>
                        <th>Date Created</th>
                        <th>Status</th>
                        <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>                             
                    </tbody>
                    </table>
                </div>
        </div>


        </main>



<!-- Modal -->
<div class="modal fade" id="empModal" tabindex="-1" role="dialog" aria-labelledby="empModalTitle" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="empModalTitle">Add New Employee</h5>

        <button type="button" class="close" data-dismiss="modal" onclick="location.reload()" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
          
            <div class="form-group main">

                <h6>Employee Information</h6>
                <div class="row">

                    <div class="col-md-12 selectApplicant">
                        <label>Applicant</label>                                                                
                        <div class="form-group">
                            <select id="selectApplicant" class="form-control">
                                <option value="">Select Applicant</option>
                            </select>
                        </div>
                    </div>                
                    <div class="col-md-12">
                        <label>Employee Name</label>                                                                
                        <div class="form-group">
                            <input id="fname" type="text" class="form-control" placeholder="Given Name"/>
                        </div>
                        <div class="form-group">
                            <input id="mname" type="text" class="form-control" placeholder="MiddleName"/>
                        </div>
                        <div class="form-group">
                            <input id="lname" type="text" class="form-control" placeholder="Sur Name"/>
                        </div>                            
                    </div>
                    <div class="col-md-12">
                        <label>Address</label>                                                                
                        <div class="form-group">
                            <input id="add" type="text" class="form-control" placeholder="Address"/>
                        </div>
                    </div>

                </div>
                <div class="row">
                    <div class="col-md-8">
                            <label>Email</label>                                                                
                            <div class="form-group">
                                <input id="email" type="email" class="form-control" placeholder="Email"/>
                            </div>
                        </div>
                    <div class="col-md-4">
                        <div class="form-group">       
                            <label>Contact Number</label>                                                                  
                            <input id="contact" type="number" class="form-control" placeholder="Contact Number"/>
                        </div>                       
                    </div>
                </div>
                <hr/>

                <h6>Company Information</h6>
                <div class="row">
                        <div class="col-md-12">
                            <label>Company Name</label>                                                                
                            <div class="form-group">
                                <input id="company" type="text" class="form-control" placeholder="Company Name"/>
                            </div>
                        </div>                
                        <div class="col-md-12">
                            <label>Employment Status</label>                                                                
                            <div class="form-group">
                                <select id="emp_status" class="form-control">
                                    <option value="">Select Status</option>
                                    <option value="Probitionary">Probitionary</option>
                                    <option value="Regular">Regular</option>
                                    <option value="Terminate">Terminate</option>
                                    <option value="Resign">Resign</option>
                                    <option value="Floating">Floating</option>
                                </select>
                            </div>
                        </div>
                </div>
                <hr/>                

                <h6>Employee Account</h6>
                <div class="row">
                    <div class="col-md-12">
                            <label>Username</label>                                                                
                            <div class="form-group">
                                <input id="user" type="text" class="form-control" placeholder="Username"/>
                            </div>
                        </div>
                    <div class="col-md-12">
                        <div class="form-group">       
                            <label>Password</label>                                                                  
                            <input id="pass" type="password" class="form-control" placeholder="Password"/>
                        </div>                       
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">       
                            <label>Confirm Password</label>                                                                  
                            <input id="cpass" type="password" class="form-control" placeholder="Confirm Password"/>
                        </div>                       
                    </div>                    
                </div>

            </div>

            
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal" onclick="location.reload()">Close</button>
        <button type="button" id="btnSave" class="btn btn-success">Create Employee Account</button>
      </div>
    </div>
  </div>
</div>




<!-- personal data tmpl -->
<div id="empData" style="display:none;">

        <div class="form-group">
            
            <h6>Personal Information</h6>
            <div class="row">
                    <div class="col-md-4">
                        <label>Given Name</label>                                                                
                        <div class="form-group">
                            <input type="text" class="form-control" placeholder="Given name"/>
                        </div>
                    </div>                    
                    <div class="col-md-4">
                        <label>Sur Name</label>                                                                                        
                        <div class="form-group">                        
                            <input type="text" class="form-control" placeholder="Sur name"/>
                        </div>
                    </div>                    
                    <div class="col-md-4">
                        <label>Middle Name</label>                                                                                        
                        <div class="form-group">                                                
                            <input type="text" class="form-control" placeholder="Middle name"/>
                        </div>
                    </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">                                                                    
                        <label>Home Address</label>                        
                        <input type="text" class="form-control" placeholder="Home Address"/>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">       
                        <label>Mobile number 1</label>                                                                  
                        <input type="number" class="form-control" placeholder="Mobile Number"/>
                    </div>                       
                </div>
                <div class="col-md-3">
                    <div class="form-group">       
                        <label>Mobile number 2</label>                                                                  
                        <input type="number" class="form-control" placeholder="Mobile Number"/>
                    </div>                       
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label>Date of Birth</label>                                                                  
                        <input type="date" class="form-control" placeholder="Date of Birth"/>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="form-group">
                        <label>Place of Birth</label>                                      
                        <input type="number" class="form-control" placeholder="Place of Birth"/>
                    </div>                       
                </div>  
            </div>
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label>Gender</label>                                                                  
                        <select class="form-control">
                            <option value="">Select Gender</option>
                            <option value="">Male</option>                            
                            <option value="">Female</option>                                                        
                        </select>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label>Citizenship</label>                                      
                        <input type="number" class="form-control" placeholder="Citizenship"/>
                    </div>                       
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label>Civil Status</label>
                        <select class="form-control">
                            <option value="">Select Civil Status</option>
                            <option value="">Single</option>                            
                            <option value="">Married</option>                                                        
                            <option value="">Widowed</option>
                        </select>
                    </div>
                </div>                
            </div>
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label>Height</label>                                                                  
                        <input type="text" class="form-control" placeholder="Height"/>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label>Weight</label>                                      
                        <input type="text" class="form-control" placeholder="Weight"/>
                    </div>                       
                </div>             
            </div>

        </div>
        <hr/>


        <div class="form-group">
        
            <h6>* Skip this if you select civil status as single</h6>
            <div class="row">
                    <div class="col-md-8">
                        <label>Name of Spouse</label>                                                                
                        <div class="form-group">
                            <input type="text" class="form-control" placeholder="Name of Spouse"/>
                        </div>
                    </div>                    
                    <div class="col-md-4">
                        <label>Occupation</label>                                                                                        
                        <div class="form-group">                        
                            <input type="text" class="form-control" placeholder="Occupation"/>
                        </div>
                    </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">       
                        <label>Mobile number 1</label>                                                                  
                        <input type="number" class="form-control" placeholder="Mobile Number"/>
                    </div>                       
                </div>
                <div class="col-md-4">
                    <div class="form-group">       
                        <label>Mobile number 2</label>                                                                  
                        <input type="number" class="form-control" placeholder="Mobile Number"/>
                    </div>                       
                </div>
            </div>
            <div class="row">
                <div class="col-md-8">
                    <label>Name of Children</label>                                                                
                    <div class="form-group">
                        <input type="text" class="form-control" placeholder="Name of Children"/>
                    </div>
                </div>                    
                <div class="col-md-4">
                    <label>Age</label>                                                                                        
                    <div class="form-group">                        
                        <input type="number" class="form-control" placeholder="Age"/>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-8">
                    <label>Name of Children</label>                                                                
                    <div class="form-group">
                        <input type="text" class="form-control" placeholder="Name of Children"/>
                    </div>
                </div>                    
                <div class="col-md-4">
                    <label>Age</label>                                                                                        
                    <div class="form-group">                        
                        <input type="number" class="form-control" placeholder="Age"/>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-8">
                    <label>Name of Children</label>                                                                
                    <div class="form-group">
                        <input type="text" class="form-control" placeholder="Name of Children"/>
                    </div>
                </div>                    
                <div class="col-md-4">
                    <label>Age</label>                                                                                        
                    <div class="form-group">                        
                        <input type="number" class="form-control" placeholder="Age"/>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-8">
                    <label>Name of Children</label>                                                                
                    <div class="form-group">
                        <input type="text" class="form-control" placeholder="Name of Children"/>
                    </div>
                </div>                    
                <div class="col-md-4">
                    <label>Age</label>                                                                                        
                    <div class="form-group">                        
                        <input type="number" class="form-control" placeholder="Age"/>
                    </div>
                </div>
            </div>                                                


        </div>            
        <hr/>


        <div class="form-group">
        
            <h6>Parent Information</h6>
            <div class="row">
                    <div class="col-md-6">
                        <label>Name of Father</label>                                                                
                        <div class="form-group">
                            <input type="text" class="form-control" placeholder="Name of Father"/>
                        </div>
                    </div>                    
                    <div class="col-md-3">
                        <label>Occupation</label>                                                                                        
                        <div class="form-group">                        
                            <input type="text" class="form-control" placeholder="Occupation"/>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">       
                            <label>Mobile number 1</label>                                                                  
                            <input type="number" class="form-control" placeholder="Mobile Number"/>
                        </div>                       
                    </div>                    
            </div>
            <div class="row">
                    <div class="col-md-6">
                        <label>Name of Mother</label>                                                                
                        <div class="form-group">
                            <input type="text" class="form-control" placeholder="Name of Mother"/>
                        </div>
                    </div>                    
                    <div class="col-md-3">
                        <label>Occupation</label>                                                                                        
                        <div class="form-group">                        
                            <input type="text" class="form-control" placeholder="Occupation"/>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">       
                            <label>Mobile number 1</label>                                                                  
                            <input type="number" class="form-control" placeholder="Mobile Number"/>
                        </div>                       
                    </div>                    
            </div>            
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">       
                        <label>Address</label>                                                                  
                        <input type="text" class="form-control" placeholder="Address"/>
                    </div>                       
                </div>
            </div>
        </div>
        <hr/>

        <div class="form-group">
        
            <h6>For Emergency</h6>
            <div class="row">
                    <div class="col-md-12">
                        <label>Person to be notified in case of Emergency</label>                                                                
                        <div class="form-group">
                            <input type="text" class="form-control" placeholder="Person to be notified in case of Emergency"/>
                        </div>
                    </div>                    
            </div>
            <div class="row">
                    <div class="col-md-8">
                        <label>Address</label>                                                                
                        <div class="form-group">
                            <input type="text" class="form-control" placeholder="Address"/>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">       
                            <label>Mobile number 1</label>                                                                  
                            <input type="number" class="form-control" placeholder="Mobile Number"/>
                        </div>                       
                    </div>                    
            </div>            


        </div>
        <hr/>

        <div class="form-group">
            
            <h6>Educational Background</h6>
            <div class="row">
                    <div class="col-md-9">
                        <label>Name of School (Primary)</label>                                            
                        <div class="form-group">
                            <input type="text" class="form-control" placeholder="Name of School (Primary)"/>
                        </div>
                    </div>                    
                    <div class="col-md-3">
                        <label>Year Attended</label>                                                                                        
                        <div class="form-group">                        
                            <input type="text" class="form-control" placeholder="Year Attended"/>
                        </div>
                    </div>
            </div>
            <div class="row">
                    <div class="col-md-9">
                        <label>Name of School (High School)</label>                                            
                        <div class="form-group">
                            <input type="text" class="form-control" placeholder="Name of School (High School)"/>
                        </div>
                    </div>                    
                    <div class="col-md-3">
                        <label>Year Attended</label>                                                                                        
                        <div class="form-group">                        
                            <input type="text" class="form-control" placeholder="Year Attended"/>
                        </div>
                    </div>                    
            </div>            
            <div class="row">
                    <div class="col-md-7">
                        <label>Name of School (College)</label>                                            
                        <div class="form-group">
                            <input type="text" class="form-control" placeholder="Name of School (College)"/>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <label>Course</label>                                            
                        <div class="form-group">
                            <input type="text" class="form-control" placeholder="Course"/>
                        </div>
                    </div>                                         
                    <div class="col-md-3">
                        <label>Year Attended</label>                                                                                        
                        <div class="form-group">                        
                            <input type="text" class="form-control" placeholder="Year Attended"/>
                        </div>
                    </div>
            </div>


        </div>
        <hr/>

        <div class="form-group">
            
            <h6>Employment History</h6>
            <div class="row">
                    <div class="col-md-5">
                        <label>Name of Company</label>                                            
                        <div class="form-group">
                            <input type="text" class="form-control" placeholder="Name of Company"/>
                        </div>
                    </div>                    
                    <div class="col-md-3">
                        <label>Position</label>                                                                                        
                        <div class="form-group">                        
                            <input type="text" class="form-control" placeholder="Position"/>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <label>From</label>                                                                                        
                        <div class="form-group">                        
                            <input type="text" class="form-control" placeholder="From"/>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <label>To</label>                                                                                        
                        <div class="form-group">                        
                            <input type="text" class="form-control" placeholder="To"/>
                        </div>
                    </div>                                                
            </div>
            <div class="row">
                    <div class="col-md-5">
                        <label>Name of Company</label>                                            
                        <div class="form-group">
                            <input type="text" class="form-control" placeholder="Name of Company"/>
                        </div>
                    </div>                    
                    <div class="col-md-3">
                        <label>Position</label>                                                                                        
                        <div class="form-group">                        
                            <input type="text" class="form-control" placeholder="Position"/>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <label>From</label>                                                                                        
                        <div class="form-group">                        
                            <input type="text" class="form-control" placeholder="From"/>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <label>To</label>                                                                                        
                        <div class="form-group">                        
                            <input type="text" class="form-control" placeholder="To"/>
                        </div>
                    </div>                                                
            </div>
            <div class="row">
                    <div class="col-md-5">
                        <label>Name of Company</label>                                            
                        <div class="form-group">
                            <input type="text" class="form-control" placeholder="Name of Company"/>
                        </div>
                    </div>                    
                    <div class="col-md-3">
                        <label>Position</label>                                                                                        
                        <div class="form-group">                        
                            <input type="text" class="form-control" placeholder="Position"/>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <label>From</label>                                                                                        
                        <div class="form-group">                        
                            <input type="text" class="form-control" placeholder="From"/>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <label>To</label>                                                                                        
                        <div class="form-group">                        
                            <input type="text" class="form-control" placeholder="To"/>
                        </div>
                    </div>                                                
            </div>
            
        </div>
        <hr/>

        <div class="form-group">
            
            <h6></h6>
            <div class="row">
                    <div class="col-md-6">
                        <label>SSS #</label>                                            
                        <div class="form-group">
                            <input type="text" class="form-control" placeholder="SSS #"/>
                        </div>
                    </div>                    
                    <div class="col-md-6">
                        <label>Phil Health #</label>                                                                                        
                        <div class="form-group">                        
                            <input type="text" class="form-control" placeholder="Phil Health #"/>
                        </div>
                    </div>                                              
            </div>
            <div class="row">
                    <div class="col-md-6">
                        <label>Pag Ibig #</label>                                            
                        <div class="form-group">
                            <input type="text" class="form-control" placeholder="Pag Ibig #"/>
                        </div>
                    </div>                    
                    <div class="col-md-6">
                        <label>TIN #</label>                                                                                        
                        <div class="form-group">                        
                            <input type="text" class="form-control" placeholder="TIN #"/>
                        </div>
                    </div>                                              
            </div>


        </div>
        <hr/>

</div>


<!-- requirement data tmpl -->
<div id="empReq" style="display:none;">

    <div class="form-group">
            
        <h6>Agency Requirements</h6>
        <div class="table-responsive">
            <table id="tbl-agency" class="table table-striped table-sm">
                <thead>
                    <tr>
                    <th>Requiremnts</th>
                    <th>Status</th>
                    <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>NBI</td>
                        <td>Pending</td>
                        <td>
                            <div class='form-group col-md-8'>
                                <select id='status' class='form-control'>
                                    <option value=''>Select Action</option>
                                    <option value='For Interview' data-id='"+ app_id +"'>Complete</option>
                                    <option value='Void Application' data-id='"+ app_id +"'>Processing</option>
                                    <option value='Void Application' data-id='"+ app_id +"'>Incomplete</option>
                                </select>
                            </div>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>

        <h6>Company Requirements</h6>
        <div class="table-responsive">
            <table id="tbl-company" class="table table-striped table-sm">
                <thead>
                    <tr>
                    <th>Requiremnts</th>
                    <th>Status</th>
                    <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>NBI</td>
                        <td>Pending</td>
                        <td>
                            <div class='form-group col-md-8'>
                                <select id='status' class='form-control'>
                                    <option value=''>Select Action</option>
                                    <option value='For Interview' data-id='"+ app_id +"'>Complete</option>
                                    <option value='Void Application' data-id='"+ app_id +"'>Processing</option>
                                    <option value='Void Application' data-id='"+ app_id +"'>Incomplete</option>
                                </select>
                            </div>
                        </td>
                    </tr>                                        
                </tbody>
            </table>
        </div>

    </div>

</div>


<?php include '../headers/dashboard-footer.php'; ?>
<script src="../assets/js/employee.js"></script>