<?php include '../headers/dashboard-header.php'; ?>
            
      
    <main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <h1 class="h2">Company Client</h1>
        <div class="btn-toolbar mb-2 mb-md-0">
        <div class="btn-group mr-2">
            <!-- <button class="btn btn-sm btn-outline-secondary">Share</button>
            <button class="btn btn-sm btn-outline-secondary">Export</button> -->
            <button class="btn btn-md btn-outline-secondary" data-toggle="modal" data-target="#cmpyModal">Add New Company Client</button>
            
        </div>
        <!-- <button class="btn btn-sm btn-outline-secondary dropdown-toggle">
            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-calendar"><rect x="3" y="4" width="18" height="18" rx="2" ry="2"></rect><line x1="16" y1="2" x2="16" y2="6"></line><line x1="8" y1="2" x2="8" y2="6"></line><line x1="3" y1="10" x2="21" y2="10"></line></svg>
            This week
        </button> -->
        </div>
    </div>
    
    <div class="">

            <div class="table-responsive">
                <table id="tbl-company" class="table table-striped table-sm">
                <thead>
                    <tr>
                    <th>Company Name</th>
                    <th>Company User</th>
                    <th>Date Created</th>
                    <th>Status</th>
                    <th>Action</th>
                    </tr>
                </thead>
                <tbody>                          
                </tbody>
                </table>
            </div>
    </div>


    </main>



<!-- Modal -->
<div class="modal fade" id="cmpyModal" tabindex="-1" role="dialog" aria-labelledby="cmpyModalTitle" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="cmpyModalTitle">Add New Company Client</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close" onclick=location.reload()>
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
          
            <div class="form-group">
            
                <h6>Company Information</h6>
                <div class="row">
                        <div class="col-md-12">
                            <label>Company Name</label>                                                                
                            <div class="form-group">
                                <input id="company_name" type="text" class="form-control" placeholder="Company Name"/>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <label>Address</label>                                                                
                            <div class="form-group">
                                <input id="company_add" type="text" class="form-control" placeholder="Address"/>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <label>Location</label>                                                                
                            <div class="form-group">
                                <input id="company_loc" type="text" class="form-control" placeholder="Location"/>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <label>Description</label>                                                                
                            <div class="form-group">
                                <textarea id="company_desc" class="form-control" placeholder="Description" rows=5></textarea>
                            </div>
                        </div>
                </div>
                <div class="row">
                    <div class="col-md-8">
                            <label>Email</label>                                                                
                            <div class="form-group">
                                <input id="company_email" type="email" class="form-control" placeholder="Email"/>
                            </div>
                        </div>
                    <div class="col-md-4">
                        <div class="form-group">       
                            <label>Contact Number</label>                                                                  
                            <input id="company_num" type="number" class="form-control" placeholder="Contact Number"/>
                        </div>                       
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                            <label>Username</label>                                                                
                            <div class="form-group">
                                <input id="company_user" type="text" class="form-control" placeholder="Username"/>
                            </div>
                        </div>
                    <div class="col-md-12">
                        <div class="form-group">       
                            <label>Password</label>                                                                  
                            <input id="company_pass" type="password" class="form-control" placeholder="Password"/>
                        </div>                       
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">       
                            <label>Confirm Password</label>                                                                  
                            <input id="company_cpass" type="password" class="form-control" placeholder="Confirm Password"/>
                        </div>                       
                    </div>                    
                </div>

            </div>
            <hr/>

            
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal" onclick=location.reload()>Close</button>
        <button id="btnSave" type="button" class="btn btn-success">Create Company Account</button>
      </div>
    </div>
  </div>
</div>

<?php include '../headers/dashboard-footer.php'; ?>


<script>
    $(document).ready(function(){

        loadData();
        
        $(document).on("click", "#btn-edit", function(){
            var id = $(this).data("id");
            var name = $(this).data("name");
            var address = $(this).data("address");
            var loc = $(this).data("loc");
            var desc = $(this).data("desc");
            var email = $(this).data("email");
            var num = $(this).data("num");
            var user = $(this).data("user");
            var status = $(this).data("status");
            var date = $(this).data("date");


            var id = $("#id").val();

            $("#company_name").val(name).attr("disabled", false);
            $("#company_add").val(address).attr("disabled", false);
            $("#company_loc").val(loc).attr("disabled", false);
            $("#company_desc").val(desc).attr("disabled", false);
            $("#company_email").val(email).attr("disabled", false);
            $("#company_num").val(num).attr("disabled", false);
            $("#company_user").val(user).attr("disabled", true);
            $("#company_pass").attr("disabled", true);
            $("#company_cpass").attr("disabled", true);


            $("#btnSave").text("Save Changes");
            $("#btnSave").show();
            $("#cmpyModalTitle").text("Update Job Post");
            $("#cmpyModal").modal("show");
        });

        $(document).on("click", "#btn-view", function(){

            var id = $(this).data("id");
            var name = $(this).data("name");
            var address = $(this).data("address");
            var loc = $(this).data("loc");
            var desc = $(this).data("desc");
            var email = $(this).data("email");
            var num = $(this).data("num");
            var user = $(this).data("user");
            var status = $(this).data("status");
            var date = $(this).data("date");


            $("#company_name").val(name).attr("disabled", true);
            $("#company_add").val(address).attr("disabled", true);
            $("#company_loc").val(loc).attr("disabled", true);
            $("#company_desc").val(desc).attr("disabled", true);
            $("#company_email").val(email).attr("disabled", true);
            $("#company_num").val(num).attr("disabled", true);
            $("#company_user").val(user).attr("disabled", true);
            $("#company_pass").attr("disabled", true);
            $("#company_cpass").attr("disabled", true);

            $("#btnSave").hide();
            $("#cmpyModalTitle").text("View Job Post");
            $("#cmpyModal").modal("show");

        });


        $(document).on("click", "#btn-delete", function(){
            alert("Calling delete function")
        });


        $("#btnSave").click(function(){

            var cmdType = $(this).text();

            var name_key = "#company_name";
            var address_key = "#company_add";
            var loc_key = "#company_loc";
            var desc_key = "#company_desc";
            var email_key = "#company_email";
            var num_key = "#company_num";
            var user_key = "#company_user";
            var pass_key = "#company_pass";
            var cpass_key = "#company_cpass";

            var id = $("#id").val();

            var name = $(name_key).val();
            var address = $(address_key).val();
            var loc = $(loc_key).val();
            var desc = $(desc_key).val();
            var email = $(email_key).val();
            var num = $(num_key).val();
            var user = $(user_key).val();
            var pass = $(pass_key).val();
            var cpass = $(cpass_key).val();

            var values = [name, address, loc, desc, email, num, user, pass, cpass];
            var keys = [name_key, address_key, loc_key, desc_key, email_key, num_key, user_key, pass_key, cpass_key];


            if (validateItems(values, keys)) {

                if (pass == cpass) {

                    switch(cmdType) {
                        case "Create Company Account":
                            companyRequests(values, null, "create");
                        break;
                        case "Save Changes":
                            companyRequests(values, id, "update");
                        break;
                        default:
                            alert("Unknown Function")
                        break;
                    }

                } else {
                    alert("Password does not match!")
                    $(pass_key).addClass("border-danger");
                    $(cpass_key).addClass("border-danger");
                }



            } else {
                alert("please fill up the empty items");
            }

        });




        function companyRequests(params, id, type) {

            var fd = new FormData();

            switch(type) {
                case "create":
                    fd.append("request", "create_company");
                break;
                case "update":
                    fd.append("id", id);                
                    // fd.append("status", params[8]);
                    fd.append("request", "edit_company");
                break;
            }

            fd.append("name", params[0]);
            fd.append("address", params[1]);
            fd.append("loc", params[2]);
            fd.append("desc", params[3]);
            fd.append("email", params[4]);
            fd.append("num", params[5]);
            fd.append("user", params[6]);
            fd.append("pass", params[7]);

            $.ajax({
                type: "POST",
                url: "../classes/Requests.php",
                data: fd,
                contentType: false,
                cache: false,
                processData: false,                  
                dataType: "json",
                success: function(res){

                    alert("response: " + res.result)
                    console.log(res);

                }, error: function() {
                    alert("error handler")
                }
            });
        }



        function loadData() {

            var fd = new FormData();
            fd.append("request", "fetch_company");

            $.ajax({
                type: "POST",
                url: "../classes/Requests.php",
                data: fd,
                contentType: false,
                cache: false,
                processData: false,                  
                dataType: "json",
                success: function(res){

                    if (res.success == true) {
                        populateData(res.result);
                    } else {
                        alert(res.result);
                    }

                    console.log(res);
                }, error: function() {
                    alert("error handler")
                }
            });
        }

        function populateData(datas) {

            var tmpl;

            if (datas.length > 0) {

                for (var i = 0; i < datas.length; i++) {

                    var id = datas[i]["company_id"];
                    var name = datas[i]["company_name"];
                    var address = datas[i]["company_address"];
                    var loc = datas[i]["company_location"];
                    var desc = datas[i]["company_desc"];
                    var email = datas[i]["company_email"];
                    var num = datas[i]["company_contact"];
                    var user = datas[i]["company_user"];
                    var status = datas[i]["status"];
                    var date = datas[i]["date_created"];

                    tmpl += "<tr>"+
                            "<td>"+ name +"</td>"+
                            "<td>"+ user +"</td>"+
                            "<td>"+ date +"</td>"+
                            "<td>"+ status +"</td>"+
                            "<td>"+
                                "<div class='form-group'>"+
                                    "<button id='btn-edit' class='btn btn-sm btn-success' "+
                                        "data-id='"+ id +"' "+
                                        "data-name='"+ name +"' "+
                                        "data-address='"+ address +"' "+
                                        "data-loc='"+ loc +"' "+
                                        "data-desc='"+ desc +"' "+
                                        "data-email='"+ email +"' "+
                                        "data-num='"+ num +"' "+
                                        "data-user='"+ user +"' "+
                                        "data-status='"+ status +"' "+
                                        "data-date='"+ date +"' >"+
                                        "<i class='fas fa-edit'></i>"+
                                    "</button> "+
                                    "<button id='btn-view' class='btn btn-sm btn-primary' "+
                                        "data-id='"+ id +"' "+
                                        "data-name='"+ name +"' "+
                                        "data-address='"+ address +"' "+
                                        "data-loc='"+ loc +"' "+
                                        "data-desc='"+ desc +"' "+
                                        "data-email='"+ email +"' "+
                                        "data-num='"+ num +"' "+
                                        "data-user='"+ user +"' "+                                        
                                        "data-status='"+ status +"' "+
                                        "data-date='"+ date +"' >"+
                                        "<i class='fas fa-eye'></i>"+
                                    "</button> "+
                                    "<button id='btn-delete' class='btn btn-sm btn-danger' "+
                                        "data-id='"+ id +"' > "+
                                        "<i class='fas fa-trash'></i>"+
                                    "</button> "+
                                "</div>"+
                            "</td>"+
                            "</tr>";

                }
                
            } else {
                Alert("No Result");
            }

            $("#tbl-company").find("tbody tr").remove().end()
            $("#tbl-company").append(tmpl);
            
        }


        function validateItems(values, keys) {

            var isNotEmpty = false;

            for (var i = 0; i < values.length; i++) {

                if (values[i] == "" || values[i] == null) {
                    $(keys[i]).addClass("border-danger");
                    isNotEmpty = false;
                } else {
                    isNotEmpty = true;
                }
            }

            return isNotEmpty;
        }

    });
</script>