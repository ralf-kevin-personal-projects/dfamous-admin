<?php include '../headers/dashboard-header.php'; ?>

      
        <main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
        <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
            <h1 class="h2">Reports</h1>
            <div class="btn-toolbar mb-2 mb-md-0">
                <div class="btn-group mr-2">
                    <!-- <button class="btn btn-sm btn-outline-secondary">Share</button> -->
                    <button class="btn btn-sm btn-outline-secondary"><i class="fas fa-download"></i> Export</button>
                    
                </div>
                <div>
                    <select id="filter" class="btn btn-md form-control">
                        <option value="">Select List</option>
                        <option value="emp-list">Employee List</option>
                        <option value="app-list">Applicants List</option>
                        <option value="comp-list">Company List</option>
                    </select>
                </div>
            </div>
        </div>
        
        <div class="">
                <div class="form-group filtering">
                    <label class="">Sort By:</label>
                    <select id="sorting" class="btn btn-md form-control">
                        <option value="">Floating</option>
                        <option value="">Probitionary</option>
                        <option value="">Regular</option>
                        <option value="">Terminated</option>
                        <option value="">Resigned</option>
                    </select>                
                </div>
                <div class="table-responsive">
                    <table id="tbl-report" class="table table-striped table-sm">
                    <thead>
                        <!-- <tr>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Applied Company</th>
                        <th>Status</th>
                        </tr> -->
                    </thead>
                    <tbody>
                        <!-- <tr>
                        <td>Angelo</td>
                        <td>angelo@gmail.com</td>
                        <td>Tony's Pawnshop</td>
                        <td>Pending</td>
                        </tr>                               -->
                    </tbody>
                    </table>
                </div>
        </div>


        </main>

<?php include '../headers/dashboard-footer.php'; ?>

<script>
    $(document).ready(function(){


        $(document).on("change", "#filter", function() {
            var list = $(this).val();            
            alert(list);
            loadList(list);
        });





        function loadList(list) {

            var fd = new FormData();

            switch(list) {
                case "emp-list":
                    fd.append("request", "fetch_emp_list");
                break;
                case "app-list":
                    fd.append("request", "fetch_app_list");
                break;
                case "comp-list":
                    fd.append("request", "fetch_comp_list");
                break;                
            }



            $.ajax({
                type: "POST",
                url: "../classes/Requests.php",
                data: fd,
                contentType: false,
                cache: false,
                processData: false,                  
                dataType: "json",
                success: function(res){

                    if (res.success == true) {
                        switch(list) {
                            case "emp-list":
                                populateEmpData(res.result);
                            break;
                            case "app-list":
                                populateAppData(res.result);
                            break;
                            case "comp-list":
                                populateCompData(res.result);
                            break;                
                        }                        

                    } else {
                        alert(res.result);
                        $("#tbl-report").find("thead tr").remove().end();
                        $("#tbl-report").find("tbody tr").remove().end();
                    }
                    
                    console.log(res)
                }, error: function() {
                    alert("error handler")
                }
            });
        }



        function populateEmpData(datas) {

            var headers;
            var tmpl;

            headers = "<tr>"+
                        "<th>First Name</th>"+
                        "<th>Middle Name</th>"+
                        "<th>Last Name</th>"+
                        "<th>Current Company</th>"+
                        "<th>Status</th>"+
                    "</tr>";

            if (datas.length > 0) {

                for (var i = 0; i < datas.length; i++) {

                    var emp_id = datas[i]["emp_id"];
                    var emp_fname = datas[i]["emp_fname"];
                    var emp_mname = datas[i]["emp_mname"];
                    var emp_lname = datas[i]["emp_lname"];
                    var emp_company = datas[i]["emp_company"];
                    var emp_status = datas[i]["emp_status"];

                    tmpl += "<tr>"+
                            "<td>"+ emp_fname +"</td>"+
                            "<td>"+ emp_mname +"</td>"+
                            "<td>"+ emp_lname +"</td>"+
                            "<td>"+ emp_company +"</td>"+
                            "<td>"+ emp_status +"</td>"+
                            "</tr>";

                }
                
            } else {
                Alert("No Result");
            }

            $("#tbl-report").find("thead tr").remove().end();
            $("#tbl-report").find("thead").append(headers);

            $("#tbl-report").find("tbody tr").remove().end();
            $("#tbl-report").append(tmpl);
            
        }

        function populateAppData(datas) {

            var headers;
            var tmpl;

            headers = "<tr>"+
                        "<th>First Name</th>"+
                        "<th>Middle Name</th>"+
                        "<th>Last Name</th>"+
                        "<th>Applied Company</th>"+
                        "<th>Status</th>"+
                        "<th>Date Applied</th>"+
                    "</tr>";

            if (datas.length > 0) {

                for (var i = 0; i < datas.length; i++) {

                    var app_id = datas[i]["app_id"];
                    var app_fname = datas[i]["app_fname"];
                    var app_mname = datas[i]["app_mname"];
                    var app_lname = datas[i]["app_lname"];
                    var company_name = datas[i]["company_name"];
                    var app_status = datas[i]["app_status"];
                    var date_created = datas[i]["date_created"];

                    tmpl += "<tr>"+
                            "<td>"+ app_fname +"</td>"+
                            "<td>"+ app_mname +"</td>"+
                            "<td>"+ app_lname +"</td>"+
                            "<td>"+ company_name +"</td>"+
                            "<td>"+ app_status +"</td>"+
                            "<td>"+ date_created +"</td>"+
                            "</tr>";

                }
                
            } else {
                Alert("No Result");
            }

            $("#tbl-report").find("thead tr").remove().end();
            $("#tbl-report").find("thead").append(headers);

            $("#tbl-report").find("tbody tr").remove().end();
            $("#tbl-report").append(tmpl);
            
        }


        function populateCompData(datas) {

            var headers;
            var tmpl;

            headers = "<tr>"+
                        "<th>Company Name</th>"+
                        "<th>Company User</th>"+
                        "<th>Status</th>"+
                        "<th>Date Created</th>"+
                    "</tr>";

            if (datas.length > 0) {

                for (var i = 0; i < datas.length; i++) {

                    var company_id = datas[i]["company_id"];
                    var company_name = datas[i]["company_name"];
                    var company_user = datas[i]["company_user"];
                    var status = datas[i]["status"];
                    var date_created = datas[i]["date_created"];

                    tmpl += "<tr>"+
                            "<td>"+ company_name +"</td>"+
                            "<td>"+ company_user +"</td>"+
                            "<td>"+ status +"</td>"+
                            "<td>"+ date_created +"</td>"+
                            "</tr>";

                }
                
            } else {
                Alert("No Result");
            }

            $("#tbl-report").find("thead tr").remove().end();
            $("#tbl-report").find("thead").append(headers);

            $("#tbl-report").find("tbody tr").remove().end();
            $("#tbl-report").append(tmpl);
            
        }


    });
</script>