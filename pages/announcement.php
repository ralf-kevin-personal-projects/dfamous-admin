<?php include '../headers/dashboard-header.php'; ?>

      
        <main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
        <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
            <h1 class="h2">Announcement</h1>
            <div class="btn-toolbar mb-2 mb-md-0">
            <div class="btn-group mr-2">
                <!-- <button class="btn btn-sm btn-outline-secondary">Share</button>
                <button class="btn btn-sm btn-outline-secondary">Export</button> -->
                <button class="btn btn-md btn-outline-secondary" data-toggle="modal" data-target="#annModal">Add New Announcement</button>
                
            </div>
            <!-- <button class="btn btn-sm btn-outline-secondary dropdown-toggle">
                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-calendar"><rect x="3" y="4" width="18" height="18" rx="2" ry="2"></rect><line x1="16" y1="2" x2="16" y2="6"></line><line x1="8" y1="2" x2="8" y2="6"></line><line x1="3" y1="10" x2="21" y2="10"></line></svg>
                This week
            </button> -->
            </div>
        </div>
        
        <div class="">

                <div class="table-responsive">
                    <table id="tbl-ann" class="table table-striped table-sm">
                    <thead>
                        <tr>
                        <th>Title</th>
                        <th>Description</th>
                        <th>Date Created</th>
                        <th>Status</th>
                        <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                        <td>Highest Paying Jobs</td>
                        <td>Lorem Ipsum dolor</td>
                        <td>2018-18-20</td>
                        <td>
                            For Approval
                        </td>
                        <td>
                            <div class="form-group">
                                <button id="btnEdit" class="btn btn-sm btn-success" onclick=edit()>
                                        <i class="fas fa-edit"></i>
                                </button>
                                <button id="btnView" class="btn btn-sm btn-primary" onclick=view()>
                                    <i class="fas fa-eye"></i>
                                </button>                                
                                <button class="btn btn-sm btn-danger"onclick=archive()>
                                    <i class="fas fa-trash"></i>
                                </button>
                            </div>
                        </td>
                        </tr>                              
                    </tbody>
                    </table>
                </div>
        </div>


        </main>



<!-- Modal -->
<div class="modal fade" id="annModal" tabindex="-1" role="dialog" aria-labelledby="annModalTitle" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="annModalTitle">Add New Announcement</h5>
        <button type="button" class="close" data-dismiss="modal" onclick="location.reload()" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
          
            <div class="form-group main">
            
                <h6>Announcement</h6>
                <div class="row">              
                    <div class="col-md-12">
                        <label>Announcement Title</label>                                                                
                        <div class="form-group">
                            <input id="ann-title" type="text" class="form-control" placeholder="Announcement Title"/>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                            <label>Description</label>                                                                
                            <div class="form-group">
                                <textarea id="ann-desc" class="form-control" placeholder="Description" rows=5></textarea>
                            </div>
                        </div>
                </div>

            </div>

            
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal" onclick="location.reload()">Close</button>
        <button type="button" id="btnSave" class="btn btn-success">Create Announcement</button>
      </div>
    </div>
  </div>
</div>


<?php include '../headers/dashboard-footer.php'; ?>

<script>
    $(document).ready(function(){

        loadData();
        
        $(document).on("click", "#btn-edit", function(){
            var id = $(this).data("id");
            var title = $(this).data("title");
            var desc = $(this).data("desc");


            $("#ann-title").val(title).attr("disabled", false);
            $("#ann-desc").val(desc).attr("disabled", false);

            $("#btnSave").text("Save Changes");
            $("#btnSave").show();
            $("#annModalTitle").text("Update Announcement");
            $("#annModal").modal("show");
        });

        $(document).on("click", "#btn-view", function(){

            var id = $(this).data("id");
            var title = $(this).data("title");
            var desc = $(this).data("desc");


            $("#ann-title").val(title).attr("disabled", false);
            $("#ann-desc").val(desc).attr("disabled", false);

            $("#btnSave").hide();
            $("#annModalTitle").text("View Announcement");
            $("#annModal").modal("show");

        });


        $(document).on("click", "#btn-delete", function(){
            alert("Calling delete function")
        });

        $(document).on("change", "#status", function() {
            var request_id = $(this).find("option:selected").data("req");
            var status = $(this).val();
            
            var values = [request_id, status];
            companyRequest(values)
        });

        function companyRequest(values) {

            var fd = new FormData();

            fd.append("request_id", values[0]);
            fd.append("status", values[1]);
            fd.append("request", "request_override");

            $.ajax({
                type: "POST",
                url: "../classes/Requests.php",
                data: fd,
                contentType: false,
                cache: false,
                processData: false,                  
                dataType: "json",
                success: function(res){

                    alert("response: " + res.result)
                    location.reload();
                    console.log(res);

                }, error: function() {
                    alert("error handler")
                }
            });
        }



        function loadData() {

            var fd = new FormData();
            fd.append("request", "fetch_ann");

            $.ajax({
                type: "POST",
                url: "../classes/Requests.php",
                data: fd,
                contentType: false,
                cache: false,
                processData: false,                  
                dataType: "json",
                success: function(res){

                    if (res.success == true) {
                        populateData(res.result);
                    } else {
                        alert(res.result);
                    }

                    console.log(res);
                }, error: function() {
                    alert("error handler")
                }
            });
        }

        function populateData(datas) {

            var tmpl;

            if (datas.length > 0) {

                for (var i = 0; i < datas.length; i++) {

                    var ann_id = datas[i]["ann_id"];
                    var ann_title = datas[i]["ann_title"];
                    var ann_desc = datas[i]["ann_desc"];
                    var ann_status = datas[i]["ann_status"];
                    var date_created = datas[i]["date_created"];

                    tmpl += "<tr>"+
                            "<td>"+ ann_title +"</td>"+
                            "<td>"+ ann_desc +"</td>"+
                            "<td>"+ ann_status +"</td>"+
                            "<td>"+ date_created +"</td>"+
                            "<td>"+
                                "<div class='form-group'>"+
                                    "<button id='btn-edit' class='btn btn-sm btn-success' "+
                                        "data-id='"+ ann_id +"' "+
                                        "data-title='"+ ann_title +"' "+
                                        "data-desc='"+ ann_desc +"' "+
                                        "data-status='"+ ann_status +"' "+
                                        "data-date='"+ date_created +"' >"+                              
                                        "<i class='fas fa-edit'></i>"+
                                    "</button> "+
                                    "<button id='btn-view' class='btn btn-sm btn-primary' "+
                                        "data-id='"+ ann_id +"' "+
                                        "data-title='"+ ann_title +"' "+
                                        "data-desc='"+ ann_desc +"' "+
                                        "data-status='"+ ann_status +"' "+
                                        "data-date='"+ date_created +"' >"+                                    
                                        "<i class='fas fa-eye'></i>"+
                                    "</button> "+                    
                                    "<button id='btn-delete' class='btn btn-sm btn-danger' "+
                                        "data-id='"+ ann_id +"' >"+
                                        "<i class='fas fa-trash'></i>"+
                                    "</button> "+
                                "</div>"+
                            "</td>"+
                            "</tr>";

                }
                
            } else {
                Alert("No Result");
            }

            $("#tbl-ann").find("tbody tr").remove().end();
            $("#tbl-ann").append(tmpl);
            
        }


        function validateItems(values, keys) {

            var isNotEmpty = false;

            for (var i = 0; i < values.length; i++) {

                if (values[i] == "" || values[i] == null) {
                    $(keys[i]).addClass("border-danger");
                    isNotEmpty = false;
                } else {
                    isNotEmpty = true;
                }
            }

            return isNotEmpty;
        }

    });
</script>
