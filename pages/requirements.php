<?php include '../headers/dashboard-header.php'; ?>

      
        <main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
        <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
            <h1 class="h2">Requirements</h1>
            <div class="btn-toolbar mb-2 mb-md-0">
            <div class="btn-group mr-2">
                <!-- <button class="btn btn-sm btn-outline-secondary">Share</button>
                <button class="btn btn-sm btn-outline-secondary">Export</button> -->
                <button class="btn btn-md btn-outline-secondary" data-toggle="modal" data-target="#reqModal" >Add New Requirements</button>
                
            </div>
            <!-- <button class="btn btn-sm btn-outline-secondary dropdown-toggle">
                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-calendar"><rect x="3" y="4" width="18" height="18" rx="2" ry="2"></rect><line x1="16" y1="2" x2="16" y2="6"></line><line x1="8" y1="2" x2="8" y2="6"></line><line x1="3" y1="10" x2="21" y2="10"></line></svg>
                This week
            </button> -->
            </div>
        </div>
        
        <div class="row">
            <div class="col-md-6 card card-body">

                    <div class="table-responsive">
                        <h5>Agency Requirements</h5>
                        <table id="tbl-admin" class="table table-striped table-sm">
                        <thead>
                            <tr>
                            <th>Requirement(s)</th>
                            <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                            <td>NBI</td>
                            <td>
                                <div class="form-group">
                                    <button class="btn btn-sm btn-danger" >
                                        <i class="fas fa-trash"></i>
                                    </button>
                                </div>
                            </td>
                            </tr>                     
                        </tbody>
                        </table>
                    </div>
            </div>

            <div class="offset-md-1 col-md-5 card card-body">
                <div class="table-responsive">
                    <h5>Company Requirements</h5>
                    <div class="form-group">
                        <select id="select-company" class="form-control">
                            <option value="">Select Company</option>
                        </select>
                    </div>
                    <table id="tbl-company" class="table table-striped table-sm">
                        <thead>
                            <tr>
                            <th>Requirement(s)</th>
                            </tr>
                        </thead>
                        <tbody>            
                        </tbody>
                    </table>
                </div>
            </div>
        </div>


        </main>



<!-- Modal -->
<div class="modal fade" id="reqModal" tabindex="-1" role="dialog" aria-labelledby="reqModalTitle" aria-hidden="true">
  <div class="modal-dialog modal-md" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="reqModalTitle">Add New Requirement</h5>
        <button type="button" class="close" data-dismiss="modal" onclick="location.reload()" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
          
            <div class="form-group main">
            
                <h6>Requirement</h6>
                <div class="row">              
                    <div class="col-md-12">
                        <label>Requirement Name</label>                                                                
                        <div class="form-group">
                            <input type="text" class="form-control" placeholder="Requirement Name"/>
                        </div>
                    </div>
                </div>
            </div>

            
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal" onclick="location.reload()">Close</button>
        <button type="button" id="btnSave" class="btn btn-success">Create Requirement</button>
      </div>
    </div>
  </div>
</div>

<?php include '../headers/dashboard-footer.php'; ?>

<script>
    $(document).ready(function(){

        loadAdminData();
        loadCompanyList();


        $(document).on("change", "#select-company", function() {
            var company_id = $(this).find("option:selected").data("id");
            
            alert(company_id);
            loadCompanyData(company_id);
        });



        function loadCompanyList() {

            var fd = new FormData();
            fd.append("request", "fetch_company_list");

            $.ajax({
                type: "POST",
                url: "../classes/Requests.php",
                data: fd,
                contentType: false,
                cache: false,
                processData: false,                  
                dataType: "json",
                success: function(res){

                    if (res.success == true) {
                        populateCompanyList(res.result);
                    } else {
                        alert(res.result);
                    }

                }, error: function() {
                    alert("error handler")
                }
            });
        }


        function loadAdminData() {

            var fd = new FormData();
            fd.append("request", "fetch_admin");

            $.ajax({
                type: "POST",
                url: "../classes/Requests.php",
                data: fd,
                contentType: false,
                cache: false,
                processData: false,                  
                dataType: "json",
                success: function(res){

                    if (res.success == true) {
                        populateAdminData(res.result);
                    } else {
                        alert(res.result);
                    }

                }, error: function() {
                    alert("error handler")
                }
            });
        }

        function populateAdminData(datas) {

            var tmpl;

            if (datas.length > 0) {

                for (var i = 0; i < datas.length; i++) {

                    var admin_req_id = datas[i]["admin_req_id"];
                    var admin_req_desc = datas[i]["admin_req_desc"];

                    tmpl += "<tr>"+
                            "<td>"+ admin_req_desc +"</td>"+
                            "<td>"+
                                "<div class='form-group'>"+
                                    "<button class='btn btn-sm btn-danger' "+
                                        "data-id="+ admin_req_id +" >"+
                                        "<i class='fas fa-trash'></i>"+
                                    "</button>"+
                                "</div>"+
                            "</td>"+
                            "</tr>";

                }
                
            } else {
                Alert("No Result");
            }

            $("#tbl-admin").find("tbody tr").remove().end();
            $("#tbl-admin").append(tmpl);
            
        }












        function populateCompanyList(datas) {

            var tmpl;

            if (datas.length > 0) {

                for (var i = 0; i < datas.length; i++) {

                    var company_id = datas[i]["company_id"];
                    var company_name = datas[i]["company_name"];

                    tmpl += "<option data-id="+ company_id +">"+ company_name +"</option>";

                }
                
            } else {
                Alert("No Result");
                $("#select-company").find("option").remove().end();                
            }

            $("#select-company").append(tmpl);
            
        }

        function loadCompanyData(id) {

            var fd = new FormData();
            fd.append("company_id", id);
            fd.append("request", "fetch_company_req");

            $.ajax({
                type: "POST",
                url: "../classes/Requests.php",
                data: fd,
                contentType: false,
                cache: false,
                processData: false,                  
                dataType: "json",
                success: function(res){

                    if (res.success == true) {
                        populateCompanyData(res.result);
                    } else {
                        alert(res.result);
                        $("#tbl-company").find("tbody tr").remove().end();
                    }

                    console.log(res);
                }, error: function() {
                    alert("error handler")
                }
            });
        }

        function populateCompanyData(datas) {

            var tmpl;

            if (datas.length > 0) {

                for (var i = 0; i < datas.length; i++) {

                    var comp_req_id = datas[i]["comp_req_id"];
                    var comp_req_desc = datas[i]["comp_req_desc"];

                    tmpl += "<tr>"+
                            "<td>"+ comp_req_desc +"</td>"+
                            "</tr>";

                }
                
            } else {
                Alert("No Result");
            }

            $("#tbl-company").find("tbody tr").remove().end();
            $("#tbl-company").append(tmpl);
            
        }


    });
</script>