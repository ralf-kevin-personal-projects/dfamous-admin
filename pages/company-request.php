<?php include '../headers/dashboard-header.php'; ?>
            
      
    <main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <h1 class="h2">Company Request</h1>
        <div class="btn-toolbar mb-2 mb-md-0">
        <div class="btn-group mr-2">
            <!-- <button class="btn btn-sm btn-outline-secondary">Share</button>
            <button class="btn btn-sm btn-outline-secondary">Export</button> -->
            <!-- <button class="btn btn-md btn-outline-secondary">Add New Company Client</button> -->
            
        </div>
        <!-- <button class="btn btn-sm btn-outline-secondary dropdown-toggle">
            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-calendar"><rect x="3" y="4" width="18" height="18" rx="2" ry="2"></rect><line x1="16" y1="2" x2="16" y2="6"></line><line x1="8" y1="2" x2="8" y2="6"></line><line x1="3" y1="10" x2="21" y2="10"></line></svg>
            This week
        </button> -->
        </div>
    </div>
    
    <div class="">

            <div class="table-responsive">
                <table id="tbl-request" class="table table-striped table-sm">
                <thead>
                    <tr>
                    <th>Company Name</th>
                    <th>Request Type</th>
                    <th>Description</th>
                    <th>Status</th>
                    <th>Action</th>
                    <th>Override</th>                    
                    </tr>
                </thead>
                <tbody>                             
                </tbody>
                </table>
            </div>
    </div>


    </main>

<?php include '../headers/dashboard-footer.php'; ?>

<script>
    $(document).ready(function(){

        loadData();
        
        $(document).on("click", "#btn-edit", function(){
            var id = $(this).data("id");
            var name = $(this).data("name");
            var address = $(this).data("address");
            var loc = $(this).data("loc");
            var desc = $(this).data("desc");
            var email = $(this).data("email");
            var num = $(this).data("num");
            var user = $(this).data("user");
            var status = $(this).data("status");
            var date = $(this).data("date");


            var id = $("#id").val();

            $("#company_name").val(name).attr("disabled", false);
            $("#company_add").val(address).attr("disabled", false);
            $("#company_loc").val(loc).attr("disabled", false);
            $("#company_desc").val(desc).attr("disabled", false);
            $("#company_email").val(email).attr("disabled", false);
            $("#company_num").val(num).attr("disabled", false);
            $("#company_user").val(user).attr("disabled", true);
            $("#company_pass").attr("disabled", true);
            $("#company_cpass").attr("disabled", true);


            $("#btnSave").text("Save Changes");
            $("#btnSave").show();
            $("#cmpyModalTitle").text("Update Job Post");
            $("#cmpyModal").modal("show");
        });

        $(document).on("click", "#btn-view", function(){

            var id = $(this).data("id");
            var name = $(this).data("name");
            var address = $(this).data("address");
            var loc = $(this).data("loc");
            var desc = $(this).data("desc");
            var email = $(this).data("email");
            var num = $(this).data("num");
            var user = $(this).data("user");
            var status = $(this).data("status");
            var date = $(this).data("date");


            $("#company_name").val(name).attr("disabled", true);
            $("#company_add").val(address).attr("disabled", true);
            $("#company_loc").val(loc).attr("disabled", true);
            $("#company_desc").val(desc).attr("disabled", true);
            $("#company_email").val(email).attr("disabled", true);
            $("#company_num").val(num).attr("disabled", true);
            $("#company_user").val(user).attr("disabled", true);
            $("#company_pass").attr("disabled", true);
            $("#company_cpass").attr("disabled", true);

            $("#btnSave").hide();
            $("#cmpyModalTitle").text("View Job Post");
            $("#cmpyModal").modal("show");

        });


        $(document).on("click", "#btn-delete", function(){
            alert("Calling delete function")
        });

        $(document).on("change", "#status", function() {
            var request_id = $(this).find("option:selected").data("req");
            var status = $(this).val();
            
            var values = [request_id, status];
            companyRequest(values)
        });

        function companyRequest(values) {

            var fd = new FormData();

            fd.append("request_id", values[0]);
            fd.append("status", values[1]);
            fd.append("request", "request_override");

            $.ajax({
                type: "POST",
                url: "../classes/Requests.php",
                data: fd,
                contentType: false,
                cache: false,
                processData: false,                  
                dataType: "json",
                success: function(res){

                    alert("response: " + res.result)
                    location.reload();
                    console.log(res);

                }, error: function() {
                    alert("error handler")
                }
            });
        }



        function loadData() {

            var fd = new FormData();
            fd.append("request", "fetch_request");

            $.ajax({
                type: "POST",
                url: "../classes/Requests.php",
                data: fd,
                contentType: false,
                cache: false,
                processData: false,                  
                dataType: "json",
                success: function(res){

                    if (res.success == true) {
                        populateData(res.result);
                    } else {
                        alert(res.result);
                    }

                    console.log(res);
                }, error: function() {
                    alert("error handler")
                }
            });
        }

        function populateData(datas) {

            var tmpl;

            if (datas.length > 0) {

                for (var i = 0; i < datas.length; i++) {

                    var company_id = datas[i]["company_id"];
                    var company_name = datas[i]["company_name"];
                    var request_id = datas[i]["request_id"];
                    var request_type = datas[i]["request_type"];
                    var request_desc = datas[i]["request_desc"];
                    var status = datas[i]["status"];
                    var date = datas[i]["date_created"];

                    tmpl += "<tr>"+
                            "<td>"+ company_name +"</td>"+
                            "<td>"+ request_type +"</td>"+
                            "<td>"+ request_desc +"</td>"+
                            "<td>"+ status +"</td>"+
                            "<td>"+
                                "<div class='form-group'>"+
                                    "<button id='btn-view' class='btn btn-md btn-primary' "+
                                        "data-company='"+ company_name +"' "+
                                        "data-req='"+ request_id +"' "+
                                        "data-type='"+ request_type +"' "+
                                        "data-desc='"+ request_desc +"' "+
                                        "data-status='"+ status +"' "+
                                        "data-date='"+ date +"' >"+
                                        "<i class='fas fa-eye'></i>"+
                                    "</button> "+
                                "</div>"+
                            "</td>"+
                            "<td>"+
                                "<div class='form-group'>"+
                                    "<select id='status' class='col-md-10 form-control' >"+
                                            "<option value=''>Select Action</option>"+
                                            "<option value='Processing' data-req='"+ request_id +"'>Processing</option>"+
                                            "<option value='Confirmed' data-req='"+ request_id +"'>Confirmed</option>"+
                                            "<option value='Void' data-req='"+ request_id +"'>Void</option>"+
                                    "</select>"+
                                "</div>"+
                            "</td>"+
                            "</tr>";

                }
                
            } else {
                Alert("No Result");
            }

            $("#tbl-request").find("tbody tr").remove().end();
            $("#tbl-request").append(tmpl);
            
        }


        function validateItems(values, keys) {

            var isNotEmpty = false;

            for (var i = 0; i < values.length; i++) {

                if (values[i] == "" || values[i] == null) {
                    $(keys[i]).addClass("border-danger");
                    isNotEmpty = false;
                } else {
                    isNotEmpty = true;
                }
            }

            return isNotEmpty;
        }

    });
</script>
