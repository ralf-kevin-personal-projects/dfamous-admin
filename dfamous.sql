-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 11, 2018 at 04:22 AM
-- Server version: 10.1.36-MariaDB
-- PHP Version: 5.6.38

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `dfamous`
--

-- --------------------------------------------------------

--
-- Table structure for table `acct_admin`
--

CREATE TABLE `acct_admin` (
  `admin_id` int(11) NOT NULL,
  `admin_user` varchar(50) NOT NULL,
  `admin_pass` varchar(50) NOT NULL,
  `admin_role` varchar(50) NOT NULL,
  `admin_status` varchar(50) NOT NULL,
  `date_created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `acct_admin`
--

INSERT INTO `acct_admin` (`admin_id`, `admin_user`, `admin_pass`, `admin_role`, `admin_status`, `date_created`) VALUES
(1, 'admin', '1a1dc91c907325c69271ddf0c944bc72', 'admin', 'Activated', '2018-11-30 23:38:10');

-- --------------------------------------------------------

--
-- Table structure for table `acct_company`
--

CREATE TABLE `acct_company` (
  `company_id` int(11) NOT NULL,
  `company_user` varchar(50) NOT NULL,
  `company_pass` varchar(100) NOT NULL,
  `company_name` varchar(200) NOT NULL,
  `company_address` varchar(200) NOT NULL,
  `company_location` varchar(100) NOT NULL,
  `company_desc` longtext NOT NULL,
  `company_email` varchar(50) NOT NULL,
  `company_contact` varchar(50) NOT NULL,
  `status` varchar(50) NOT NULL DEFAULT 'Activated',
  `date_created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `acct_company`
--

INSERT INTO `acct_company` (`company_id`, `company_user`, `company_pass`, `company_name`, `company_address`, `company_location`, `company_desc`, `company_email`, `company_contact`, `status`, `date_created`) VALUES
(1, 'user', '1a1dc91c907325c69271ddf0c944bc72', 'Tonys Max Man Inc. (From A-Z)', '#123 Maligaya St., Brgy. Dutdutan, Masukal City', 'Masukal City, Philippines', 'This company started from a man who gives mr. Tony an unlimited power and supply to conquer spaces and cosmic realm. This Max Man supplement will lead you to actual heaven (not the heaven from the bible but from heavenly bodies).', 'tonymaxman@maxmaninc.com', '09092212234', 'Activated', '2018-11-30 13:46:02'),
(3, 'user', 'ee11cbb19052e40b07aac0ca060c23ee', 'sample company', 'address', 'location', 'desc', 'email', '1212', 'Activated', '2018-12-01 11:24:45'),
(4, 'baliktad96', '4d702022947b6fed64518d0d7cfc692d', '96', '96 ave.', 'ave st.', '96', '96@mail.com', '0912312332', 'Activated', '2018-12-05 21:59:51'),
(5, 'bago96', '81dc9bdb52d04dc20036dbd8313ed055', 'bago', 'new', 'new st.', 'bago to...', 'bago@mail.com', '0909090909', 'Activated', '2018-12-05 22:03:54');

-- --------------------------------------------------------

--
-- Table structure for table `acct_emp`
--

CREATE TABLE `acct_emp` (
  `emp_id` int(11) NOT NULL,
  `emp_user` varchar(50) NOT NULL,
  `emp_pass` varchar(50) NOT NULL,
  `emp_email` varchar(50) NOT NULL,
  `status` varchar(50) NOT NULL,
  `date_created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `acct_emp`
--

INSERT INTO `acct_emp` (`emp_id`, `emp_user`, `emp_pass`, `emp_email`, `status`, `date_created`) VALUES
(1, 'emp1', 'pass', '', 'Active', '2018-12-08 18:35:46');

-- --------------------------------------------------------

--
-- Table structure for table `admin_req`
--

CREATE TABLE `admin_req` (
  `admin_req_id` int(11) NOT NULL,
  `admin_req_desc` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `announcement`
--

CREATE TABLE `announcement` (
  `ann_id` int(11) NOT NULL,
  `ann_title` varchar(100) NOT NULL,
  `ann_desc` longtext NOT NULL,
  `ann_status` varchar(50) NOT NULL DEFAULT 'Pending',
  `date_created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `applicant`
--

CREATE TABLE `applicant` (
  `app_id` int(11) NOT NULL,
  `app_fname` varchar(100) NOT NULL,
  `app_mname` varchar(100) NOT NULL,
  `app_lname` varchar(100) NOT NULL,
  `app_address` varchar(200) NOT NULL DEFAULT 'to follow',
  `app_mobile1` varchar(100) NOT NULL DEFAULT 'to follow',
  `app_mobile2` varchar(100) NOT NULL DEFAULT 'to follow',
  `app_bday` varchar(100) NOT NULL DEFAULT 'to follow',
  `app_bplace` varchar(200) NOT NULL DEFAULT 'to follow',
  `app_gender` varchar(50) NOT NULL DEFAULT 'to follow',
  `app_citizenship` varchar(100) NOT NULL DEFAULT 'to follow',
  `app_civil` varchar(50) NOT NULL DEFAULT 'to follow',
  `app_height` varchar(50) NOT NULL DEFAULT 'to follow',
  `app_weight` varchar(50) NOT NULL DEFAULT 'to follow',
  `app_spouse` varchar(100) NOT NULL DEFAULT 'to follow',
  `app_spouse_occupation` varchar(100) NOT NULL DEFAULT 'to follow',
  `app_spouse_mobile1` varchar(50) NOT NULL DEFAULT 'to follow',
  `app_spouse_mobile2` varchar(50) NOT NULL DEFAULT 'to follow',
  `app_children1_name` varchar(100) NOT NULL DEFAULT 'to follow',
  `app_children1_age` varchar(50) NOT NULL DEFAULT 'to follow',
  `app_children2_name` varchar(100) NOT NULL DEFAULT 'to follow',
  `app_children2_age` varchar(50) NOT NULL DEFAULT 'to follow',
  `app_children3_name` varchar(100) NOT NULL DEFAULT 'to follow',
  `app_children3_age` varchar(50) NOT NULL DEFAULT 'to follow',
  `app_children4_name` varchar(50) NOT NULL DEFAULT 'to follow',
  `app_children4_age` varchar(50) NOT NULL DEFAULT 'to follow',
  `app_father` varchar(100) NOT NULL DEFAULT 'to follow',
  `app_father_occupation` varchar(50) NOT NULL DEFAULT 'to follow',
  `app_father_mobile` varchar(50) NOT NULL DEFAULT 'to follow',
  `app_mother` varchar(100) NOT NULL DEFAULT 'to follow',
  `app_mother_occupation` varchar(50) NOT NULL DEFAULT 'to follow',
  `app_mother_mobile` varchar(50) NOT NULL DEFAULT 'to follow',
  `app_parent_add` varchar(200) NOT NULL DEFAULT 'to follow',
  `app_emergency_person` varchar(100) NOT NULL DEFAULT 'to follow',
  `app_emergency_add` varchar(200) NOT NULL DEFAULT 'to follow',
  `app_emergency_mobile` varchar(50) NOT NULL DEFAULT 'to follow',
  `app_schl_pri` varchar(100) NOT NULL DEFAULT 'to follow',
  `app_schl_pri_yr` varchar(50) NOT NULL DEFAULT 'to follow',
  `app_schl_hs` varchar(100) NOT NULL DEFAULT 'to follow',
  `app_schl_hs_yr` varchar(50) NOT NULL DEFAULT 'to follow',
  `app_schl_college` varchar(100) NOT NULL DEFAULT 'to follow',
  `app_schl_course` varchar(100) NOT NULL DEFAULT 'to follow',
  `app_schl_course_yr` varchar(50) NOT NULL DEFAULT 'to follow',
  `app_cmpny1` varchar(100) NOT NULL DEFAULT 'to follow',
  `app_cmpny1_pos` varchar(50) NOT NULL DEFAULT 'to follow',
  `app_cmpny1_from` varchar(50) NOT NULL DEFAULT 'to follow',
  `app_cmpny1_to` varchar(50) NOT NULL DEFAULT 'to follow',
  `app_cmpny2` varchar(100) NOT NULL DEFAULT 'to follow',
  `app_cmpny2_pos` varchar(50) NOT NULL DEFAULT 'to follow',
  `app_cmpny2_from` varchar(50) NOT NULL DEFAULT 'to follow',
  `app_cmpny2_to` varchar(50) NOT NULL DEFAULT 'to follow',
  `app_cmpny3` varchar(100) NOT NULL DEFAULT 'to follow',
  `app_cmpny3_pos` varchar(50) NOT NULL DEFAULT 'to follow',
  `app_cmpny3_from` varchar(50) NOT NULL DEFAULT 'to follow',
  `app_cmpny3_to` varchar(50) NOT NULL DEFAULT 'to follow',
  `app_sss` varchar(100) NOT NULL DEFAULT 'to follow',
  `app_pagibig` varchar(100) NOT NULL DEFAULT 'to follow',
  `app_philhealth` varchar(100) NOT NULL DEFAULT 'to follow',
  `app_tin` varchar(100) NOT NULL DEFAULT 'to follow',
  `app_email` varchar(50) NOT NULL,
  `app_cv` varchar(50) NOT NULL,
  `company_id` varchar(50) NOT NULL,
  `date_created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `company_posting`
--

CREATE TABLE `company_posting` (
  `post_id` int(11) NOT NULL,
  `post_title` varchar(50) NOT NULL,
  `post_desc` varchar(50) NOT NULL,
  `post_educ` varchar(200) NOT NULL,
  `post_exp` varchar(50) NOT NULL,
  `post_emp_status` varchar(50) NOT NULL,
  `post_salary` varchar(50) NOT NULL,
  `post_cat` varchar(50) NOT NULL,
  `post_vacancy` int(11) NOT NULL,
  `post_status` varchar(50) NOT NULL DEFAULT 'Pending',
  `publish_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `company_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `company_posting`
--

INSERT INTO `company_posting` (`post_id`, `post_title`, `post_desc`, `post_educ`, `post_exp`, `post_emp_status`, `post_salary`, `post_cat`, `post_vacancy`, `post_status`, `publish_date`, `company_id`) VALUES
(1, 'Hiring', 'desc', 'BSIT', '1-3yrs', '', '30,000', 'IT', 1, 'Approved', '2018-11-28 00:28:55', 1),
(2, 'Hiring QA specialists', 'desc', 'BSIT or equivalent', 'At least 1yr', '', '100000', 'job', 111, 'Approved', '2018-11-28 00:29:51', 1),
(3, 'title3', 'desc', 'educ3', 'exper3', '', '20000-30000', 'job', 12, 'Disapproved', '2018-11-28 01:04:46', 1),
(4, 'title4', 'desc', 'BSIT', '1-3yrs', '', '30000-400000', 'IT', 12, 'Disapproved', '2018-11-28 01:06:03', 1),
(5, 'title 4.2', 'desc', 'BSCS', 'entry', '', '12000', 'IT', 12, 'Disapproved', '2018-11-28 01:06:54', 1),
(6, 'HIRING SOFTWARE ENGINEER', 'Apply and be part of our minions', 'BSIT or Equivalent', 'Entry Level', '', '15000 - 25000 php', 'IT SOFTWARE', 10, 'Approved', '2018-11-28 15:08:02', 1),
(7, 'Hiring IT Support', 'Hi we are company A', 'BSIT or equivalent', 'Entry Level/Fresh Grad', '', '12000 - 15000 php', 'IT Support', 10, 'Approved', '2018-11-30 04:36:32', 1),
(8, 'Hiring Salesman for Max man', 'IKAW NAAA', 'College Grad with Long birdie', 'missionary', '', '15000 (commission based)', 'IKAW ANG NATATANGI', 1, 'Approved', '2018-11-30 14:20:39', 1),
(9, 'Sawada', 'slurp', 'buko', '1', '', '21000-50000', 'blow', 2, 'Disapproved', '2018-11-30 20:38:42', 1),
(10, 'Ricky&#39;s maxman', 'buko juice open minded kaba tara?', 'bsit', 'basta malaki', '', '10000-20000', 'basta may motor', 2, 'Pending', '2018-12-01 00:35:16', 1),
(11, 'HIRING BARTENDERS', 'ITS BA-HAR-TENDER, MAY BARIL WALANG BALA, MAY BULS', 'high school grad', 'entry level', '', '10000', 'BARTENDER', 20, 'Approved', '2018-12-01 11:42:23', 3),
(12, 'Bagong IT', 'Di luma...', 'BSIT', 'IT eggspert', '', '96, 969', 'IT', 69, 'Disapproved', '2018-12-05 22:06:08', 5);

-- --------------------------------------------------------

--
-- Table structure for table `company_req`
--

CREATE TABLE `company_req` (
  `comp_req_id` int(11) NOT NULL,
  `comp_req_desc` varchar(50) NOT NULL,
  `company_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `company_req`
--

INSERT INTO `company_req` (`comp_req_id`, `comp_req_desc`, `company_id`) VALUES
(5, 'SSS', 3),
(6, 'POLICE CLEARANCE', 1),
(7, 'MEDICAL CERTIFICATE', 1),
(8, 'SSS', 5);

-- --------------------------------------------------------

--
-- Table structure for table `company_request`
--

CREATE TABLE `company_request` (
  `request_id` int(11) NOT NULL,
  `request_type` varchar(50) NOT NULL,
  `request_desc` longtext NOT NULL,
  `status` varchar(50) NOT NULL DEFAULT 'Pending',
  `date_created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `company_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `company_request`
--

INSERT INTO `company_request` (`request_id`, `request_type`, `request_desc`, `status`, `date_created`, `company_id`) VALUES
(1, 'Additional Employee', 'Hi Good Day Dfamous agency, we would like to request 10 applicants for our IT staff position. Thank you!', 'Pending', '2018-11-28 16:28:05', 0),
(2, 'Additional Employee', 'Dummy Text', 'Pending', '2018-11-28 16:28:50', 0),
(3, 'Terminate', 'Dummy Text', 'Pending', '2018-11-28 16:30:09', 0),
(4, 'Exchange', 'Dummy Text', 'Pending', '2018-11-28 16:31:04', 0),
(5, 'Terminate', 'Dummy Text Data', 'Pending', '2018-11-28 16:55:13', 0),
(6, 'Additional Employee', 'YUNG MABANGO SANA', 'Pending', '2018-12-01 11:47:19', 1),
(7, 'Additional Employee', 'YUNG PINAKA MABANGONG BARTENDER PLEASEEE', 'Pending', '2018-12-01 11:52:10', 3),
(8, 'Additional Employee', 'Kulang ung bago... luma na ung iba...', 'Pending', '2018-12-05 22:16:11', 5);

-- --------------------------------------------------------

--
-- Table structure for table `emp_career`
--

CREATE TABLE `emp_career` (
  `carrer_id` int(10) NOT NULL,
  `carrer_prof` varchar(50) NOT NULL,
  `carrer_co` varchar(50) NOT NULL,
  `carrer_yr` varchar(50) NOT NULL,
  `carrer_desc` varchar(50) NOT NULL,
  `emp_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `emp_details`
--

CREATE TABLE `emp_details` (
  `details_id` int(10) NOT NULL,
  `emp_fname` int(50) NOT NULL,
  `emp_mname` int(50) NOT NULL,
  `emp_lname` int(50) NOT NULL,
  `emp_status` int(50) NOT NULL,
  `company_id` varchar(50) NOT NULL,
  `emp_id` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `emp_educ`
--

CREATE TABLE `emp_educ` (
  `educ_id` int(10) NOT NULL,
  `educ_type` varchar(50) NOT NULL,
  `educ_schl` varchar(50) NOT NULL,
  `educ_yr` varchar(50) NOT NULL,
  `educ_honors` varchar(50) NOT NULL,
  `emp_id` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `emp_req`
--

CREATE TABLE `emp_req` (
  `emp_req_id` int(10) NOT NULL,
  `company_req_id` varchar(50) NOT NULL,
  `admin_req_id` varchar(50) NOT NULL,
  `status` varchar(50) NOT NULL,
  `emp_id` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `emp_request`
--

CREATE TABLE `emp_request` (
  `request_id` int(11) NOT NULL,
  `request_type` varchar(50) NOT NULL,
  `request_desc` text NOT NULL,
  `date_created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` varchar(50) NOT NULL,
  `emp_id` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `emp_request`
--

INSERT INTO `emp_request` (`request_id`, `request_type`, `request_desc`, `date_created`, `status`, `emp_id`) VALUES
(1, 'Reassignment', 'lorem ipsum dolor color yellow', '0000-00-00 00:00:00', 'Pending', '1'),
(2, 'Leave', 'lorem ipsum dolor color yellow', '2018-12-09 00:00:00', 'Pending', '1'),
(3, 'qwerty', 'asdaggadgdfbxbsdgadfadga', '2018-12-09 03:59:21', 'pending', '1');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `acct_admin`
--
ALTER TABLE `acct_admin`
  ADD PRIMARY KEY (`admin_id`);

--
-- Indexes for table `acct_company`
--
ALTER TABLE `acct_company`
  ADD PRIMARY KEY (`company_id`);

--
-- Indexes for table `acct_emp`
--
ALTER TABLE `acct_emp`
  ADD PRIMARY KEY (`emp_id`);

--
-- Indexes for table `admin_req`
--
ALTER TABLE `admin_req`
  ADD PRIMARY KEY (`admin_req_id`);

--
-- Indexes for table `announcement`
--
ALTER TABLE `announcement`
  ADD PRIMARY KEY (`ann_id`);

--
-- Indexes for table `applicant`
--
ALTER TABLE `applicant`
  ADD PRIMARY KEY (`app_id`);

--
-- Indexes for table `company_posting`
--
ALTER TABLE `company_posting`
  ADD PRIMARY KEY (`post_id`);

--
-- Indexes for table `company_req`
--
ALTER TABLE `company_req`
  ADD PRIMARY KEY (`comp_req_id`);

--
-- Indexes for table `company_request`
--
ALTER TABLE `company_request`
  ADD PRIMARY KEY (`request_id`);

--
-- Indexes for table `emp_request`
--
ALTER TABLE `emp_request`
  ADD PRIMARY KEY (`request_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `acct_admin`
--
ALTER TABLE `acct_admin`
  MODIFY `admin_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `acct_company`
--
ALTER TABLE `acct_company`
  MODIFY `company_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `acct_emp`
--
ALTER TABLE `acct_emp`
  MODIFY `emp_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `admin_req`
--
ALTER TABLE `admin_req`
  MODIFY `admin_req_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `announcement`
--
ALTER TABLE `announcement`
  MODIFY `ann_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `applicant`
--
ALTER TABLE `applicant`
  MODIFY `app_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `company_posting`
--
ALTER TABLE `company_posting`
  MODIFY `post_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `company_req`
--
ALTER TABLE `company_req`
  MODIFY `comp_req_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `company_request`
--
ALTER TABLE `company_request`
  MODIFY `request_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `emp_request`
--
ALTER TABLE `emp_request`
  MODIFY `request_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
