$(document).ready(function(){


    loadApplicant();
    loadEmployee();

    var modalContainer = $(" .modal-body");
    var modal = $("#empModal");
    var modalTitle = $("#empModalTitle");
    var modalSave = $("#btnSave");
    var empDataTmpl = "";

    $("#selectApplicant").change(function(){
        var app_id = $(this).find("option:selected").val(); 
        var fname = $(this).find("option:selected").data("fname");
        var mname = $(this).find("option:selected").data("mname");
        var lname = $(this).find("option:selected").data("lname");
        var add = $(this).find("option:selected").data("add");
        var mobile = $(this).find("option:selected").data("mobile");
        var company = $(this).find("option:selected").data("company");

        localStorage.setItem("app_id", app_id );
        $("#fname").val(fname);
        $("#mname").val(mname);
        $("#lname").val(lname);
        $("#add").val(add);
        $("#contact").val(mobile);
        $("#company").val(company);
    });

    $(document).on("click", "#btnEdit", function(){

        modalContainer.find(".selectApplicant").remove().end();
        modalSave.text("Save Changes");
        modalTitle.text("Edit Employee");
        modal.modal("show");

    });

    $(document).on("click", "#btnView", function(){

        modalContainer.find(".selectApplicant").remove().end();

        modalSave.hide();
        modalTitle.text("View Employee Account");
        modal.modal("show");

    });

    $(document).on("click", "#btnViewApp", function(){

        empDataTmpl = $("#empData").html();

        modalContainer.find(".main").remove().end();
        modalContainer.append(empDataTmpl);
        modalSave.hide();
        modalTitle.text("View Employee Data");

        modal.modal("show");

    });

    $(document).on("click", "#btnViewReq", function(){
        empDataTmpl = $("#empReq").html();

        modalContainer.find(".main").remove().end();
        modalContainer.append(empDataTmpl);
        modalSave.hide();
        modalTitle.text("View Employee Requirements");

        modal.modal("show");
    });


    $("#btnSave").click(function(){

        var value = $(this).text();

        var fname_key = "#fname";
        var mname_key = "#mname";
        var lname_key = "#lname";
        var add_key = "#add";
        var email_key = "#email";
        var contact_key = "#contact";
        var company_key = "#company";
        var emp_status_key = "#emp_status";
        var user_key = "#user";
        var pass_key = "#pass";
        var cpass_key = "#cpass";

        var fname = $("#fname").val();
        var mname = $("#mname").val();
        var lname = $("#lname").val();
        var add = $("#add").val();
        var email = $("#email").val();
        var contact = $("#contact").val();
        var company = $("#company").val();
        var emp_status = $("#emp_status").val();
        var user = $("#user").val();
        var pass = $("#pass").val();
        var cpass = $("#cpass").val();


        var values = [fname, mname, lname, add, email, contact, company, emp_status, user, pass, cpass];
        var keys = [fname_key, mname_key, lname_key, add_key, email_key, contact_key, company_key, emp_status_key, user_key, pass_key, cpass_key];

        switch(value) {
            case "Create Employee Account":
                if (validateItems(values, keys)){

                    if (pass == cpass) {
                        empRequest(values, "create_emp");
                    } else {
                        alert("Password doest not match")
                    }

                } else {
                    alert("Please fill up all the empty fields")
                }

            break;
            case "Save Changes":
                if (validateItems(values, keys)){
                    alert("saving changes")
                } else {
                    alert("Please fill up all the empty fields")
                }
            break;
        }
    });


    function empRequest(values, request) {
        var fd = new FormData();
        var app_id = localStorage.getItem("app_id");

        fd.append("fname", values[0]);
        fd.append("mname", values[1]);
        fd.append("lname", values[2]);
        fd.append("add", values[3]);
        fd.append("email", values[4]);
        fd.append("contact", values[5]);
        fd.append("company", values[6]);
        fd.append("emp_status", values[7]);
        fd.append("user", values[8]);
        fd.append("pass", values[9]);
        fd.append("app_id", app_id);

        fd.append("request", "create_emp");

        $.ajax({
            type: "POST",
            url: "../classes/Requests.php",
            data: fd,
            contentType: false,
            cache: false,
            processData: false,                  
            dataType: "json",
            success: function(res){
                alert("response: " + res.result)
                location.reload();
                console.log(res);
            }, error: function() {
                alert("error handler")
            }
        });

    }


    function loadEmployee() {
        
        var fd = new FormData();            
        fd.append("request", "fetch_emp");

        $.ajax({
            type: "POST",
            url: "../classes/Requests.php",
            data: fd,
            contentType: false,
            cache: false,
            processData: false,                  
            dataType: "json",
            success: function(res){
                if (res.success == true) {
                    populateData(res.result);
                } else {
                    alert(res.result);
                }

                console.log(res);
            }, error: function() {
                alert("error handler")
            }
        });
    }

    function loadApplicant() {
        
        var fd = new FormData();            
        fd.append("request", "fetch_accepted_app");

        $.ajax({
            type: "POST",
            url: "../classes/Requests.php",
            data: fd,
            contentType: false,
            cache: false,
            processData: false,                  
            dataType: "json",
            success: function(res){

                if (res.success == true) {
                    populateDropDown(res.result);
                } else {
                    alert("No Avaible Applicant")
                }

                console.log(res);
            }, error: function() {
                alert("error handler")
            }
        });
    }

    function populateDropDown(datas) {

        var tmpl;

        if (datas.length > 0) {

            for (var i = 0; i < datas.length; i++) {

                var app_id = datas[i]["app_id"];
                var app_fname = datas[i]["app_fname"];
                var app_mname = datas[i]["app_mname"];
                var app_lname = datas[i]["app_lname"];
                var app_add = datas[i]["app_address"];
                var app_mobile = datas[i]["app_mobile1"];
                var company_name = datas[i]["company_name"];

                var full_name = app_fname + " " + app_lname;
                var items = [app_fname, app_lname, app_id]

                tmpl += "<option value="+ app_id +" "+
                        "data-fname='"+ app_fname +"' "+ 
                        "data-mname='"+ app_mname +"' "+ 
                        "data-lname='"+ app_lname +"' "+ 
                        "data-mobile='"+ app_mobile +"' "+ 
                        "data-add='"+ app_add +"' "+ 
                        "data-company='"+ company_name +"' >"+ 
                        full_name +"</option>";

            }
            
        } else {
            Alert("No Result");
        }

        $("#selectApplicant").find("select option").remove().end();
        $("#selectApplicant").append(tmpl);
        
    }


    function populateData(datas) {

        var tmpl;

        if (datas.length > 0) {

            for (var i = 0; i < datas.length; i++) {

                var emp_fname = datas[i]["emp_fname"];
                var emp_lname = datas[i]["emp_lname"];
                var emp_mname = datas[i]["emp_mname"];
                var emp_status = datas[i]["emp_status"];
                var status = datas[i]["status"];
                var date_created = datas[i]["date_created"];

                var full_name = emp_fname + " " + emp_lname;

                tmpl += "<tr>"+
                        "<td>"+ full_name +"</td>"+
                        "<td>"+ emp_status  +"</td>"+
                        "<td>"+ date_created +"</td>"+
                        "<td>"+ status +"</td>"+
                        "<td>"+
                            "<div class='form-group'>"+
                                "<button id='btnEdit' class='btn btn-sm btn-success'>"+
                                        "<i class='fas fa-edit'></i>"+
                                "</button> "+
                                "<button id='btnView' class='btn btn-sm btn-primary'>"+
                                    "<i class='fas fa-eye'></i>"+
                                "</button> "+
                                "<button id='btnViewApp' class='btn btn-sm btn-warning'>"+
                                    "<i class='fas fa-folder'></i>"+
                                "</button> "+
                                "<button id='btnViewReq' class='btn btn-sm btn-info'>"+
                                    "<i class='fas fa-list'></i>"+
                                "</button> "+                                
                                "<button id='btnArchive' class='btn btn-sm btn-danger'>"+
                                    "<i class='fas fa-trash'></i>"+
                                "</button>"+
                            "</div>"+
                        "</td>"+
                        "<td>"+
                        "</td>"+
                        "</tr>";

            }
            
        } else {
            Alert("No Result");
        }

        $("#tbl-emp").find("tbody tr").remove().end();
        $("#tbl-emp").append(tmpl);
        
    }


    function validateItems(values, keys) {

        var isNotEmpty = false;

        for (var i = 0; i < values.length; i++) {

            if (values[i] == "" || values[i] == null) {
                $(keys[i]).addClass("border-danger");
                isNotEmpty = false;
            } else {
                isNotEmpty = true;
            }
        }

        return isNotEmpty;
    }
});