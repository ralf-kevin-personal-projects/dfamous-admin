<?php

class Applicant extends Database {

    private $app_id;
    private $status;

    private $res;

    public function __construct($args, $req_code) {
        switch ($req_code) {
            case "fetch_applicant":
            break;
            case "app_override":
                $this->app_id = $args["app_id"];
                $this->status = $args["app_status"];
            break;
            default:

            break;
        }
    }


    public function override() {
       
        $this->createConn();

        $this->query("UPDATE applicant SET app_status = '". $this->status ."'
                    WHERE app_id = '". $this->app_id ."' ");

        $hasResult = $this->updateData();

        if ($hasResult["success"] == true) {

            $this->res["success"] = true;
            $this->res["result"] = $hasResult["result"];

        } else {

            $this->res["success"] = false;
            $this->res["result"] = $hasResult["result"];

        }

        return $this->res;

    }



    public function fetchAll() {

        $this->createConn();

        $this->query("SELECT * FROM applicant as app LEFT JOIN acct_company as cmpny ON app.company_id = cmpny.company_id ORDER BY app_id DESC ");

        $hasResult = $this->resultSet();

        if ($hasResult["success"] == true) {

            $this->res["success"] = true;
            $this->res["result"] = $hasResult["result"];

        } else {

            $this->res["success"] = false;
            $this->res["result"] = $hasResult["result"];

        }

        return $this->res;

    }


    public function countApplicants() {

        $this->createConn();

        $this->query("SELECT count(app_id) as count FROM applicant WHERE app_status = 'Pending' ");

        $hasResult = $this->resultSet();

        if ($hasResult["success"] == true) {

            $this->res["success"] = true;
            $this->res["result"] = $hasResult["result"];

        } else {

            $this->res["success"] = false;
            $this->res["result"] = "0";

        }

        return $this->res;

    }    

}