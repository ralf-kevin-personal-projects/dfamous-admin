<?php

class Employee extends Database {

    private $id;

    private $fname;
    private $mname;
    private $lname;
    private $add;
    private $email;
    private $contact;
    private $company;
    private $emp_status;
    private $user;
    private $pass;
    private $app_id;

    private $hashPass;



    private $res;

    public function __construct($args, $req_code) {
        switch ($req_code) {
            case "fetch_accepted_app":
            break;
            case "create_emp":
                $this->fname = $args["fname"];
                $this->mname = $args["mname"];
                $this->lname = $args["lname"];
                $this->add = $args["add"];
                $this->email = $args["email"];
                $this->contact = $args["contact"];
                $this->company = $args["company"];
                $this->emp_status = $args["emp_status"];
                $this->user = $args["user"];
                $this->pass = $args["pass"];
                $this->app_id = $args["app_id"];
            break;

            default:

            break;
        }
    }


    public function createAccount() {
        
        $this->createConn();

        $this->hashPass = md5($this->pass);

        $this->query("INSERT INTO acct_emp 
                    (emp_fname, emp_mname, emp_lname, emp_add, 
                    emp_email, emp_contact, emp_company, emp_status, 
                    emp_user, emp_pass, app_id)
                    VALUES
                    ( '". $this->fname ."', '". $this->mname ."', '". $this->lname ."', '". $this->add ."',
                    '". $this->email ."', '". $this->contact ."', '". $this->company ."', '". $this->emp_status ."',
                    '". $this->user ."', '". $this->hashPass ."', '". $this->app_id ."' ) ");

        $hasResult = $this->insertData();

        if ($hasResult["success"] == true) {

            $this->updateApplicantStatus();

            $this->res["success"] = true;
            $this->res["result"] = $hasResult["result"];

        } else {

            $this->res["success"] = false;
            $this->res["result"] = $hasResult["result"];

        }
        
        return $this->res;
    }

    private function updateApplicantStatus() {

        $this->createConn();

        $this->query("UPDATE applicant SET app_employed = '1' WHERE app_id = '". $this->app_id ."' ");

        $hasResult = $this->insertData();

        if ($hasResult["success"] == true) {

        $this->res["success"] = true;
        $this->res["result"] = $hasResult["result"];

        } else {

        $this->res["success"] = false;
        $this->res["result"] = $hasResult["result"];

        }

        return $this->res;
    }


    public function fetchApplicant() {

        $this->createConn();

        $this->query("SELECT * FROM applicant as app LEFT JOIN acct_company as cmpny ON app.company_id = cmpny.company_id
        WHERE app.app_status = 'Accepted' AND app.app_employed = '0' ORDER BY app_id DESC
        ");

        $hasResult = $this->resultSet();

        if ($hasResult["success"] == true) {

            $this->res["success"] = true;
            $this->res["result"] = $hasResult["result"];

        } else {

            $this->res["success"] = false;
            $this->res["result"] = $hasResult["result"];

        }

        return $this->res;

    }

    public function fetchEmployee() {

        $this->createConn();

        $this->query("SELECT * FROM acct_emp ORDER BY emp_id DESC");

        $hasResult = $this->resultSet();

        if ($hasResult["success"] == true) {

            $this->res["success"] = true;
            $this->res["result"] = $hasResult["result"];

        } else {

            $this->res["success"] = false;
            $this->res["result"] = $hasResult["result"];

        }

        return $this->res;

    }

}