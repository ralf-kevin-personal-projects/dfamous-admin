<?php

class Requirements extends Database {

    private $company_id;

    private $res;

    public function __construct($args, $req_code) {
        switch ($req_code) {
            case "fetch_admin":
            break;
            case "fetch_company_info":
            break;
            case "fetch_company_req":
                $this->company_id = $args["company_id"];
            break;            
            default:

            break;
        }
    }




    public function fetchAdminSide() {

        $this->createConn();

        $this->query("SELECT * FROM admin_req ORDER BY admin_req_id DESC ");

        $hasResult = $this->resultSet();

        if ($hasResult["success"] == true) {

            $this->res["success"] = true;
            $this->res["result"] = $hasResult["result"];

        } else {

            $this->res["success"] = false;
            $this->res["result"] = $hasResult["result"];

        }

        return $this->res;

    }


    public function fetchCompanyList() {

        $this->createConn();

        $this->query("SELECT company_id, company_name FROM acct_company WHERE status = 'Activated' ORDER BY company_id ASC ");

        $hasResult = $this->resultSet();

        if ($hasResult["success"] == true) {

            $this->res["success"] = true;
            $this->res["result"] = $hasResult["result"];

        } else {

            $this->res["success"] = false;
            $this->res["result"] = $hasResult["result"];

        }

        return $this->res;

    }

    public function fetchCompanySide() {

        $this->createConn();

        $this->query("SELECT * FROM company_req WHERE company_id = '". $this->company_id ."' ORDER BY comp_req_id DESC ");

        $hasResult = $this->resultSet();

        if ($hasResult["success"] == true) {

            $this->res["success"] = true;
            $this->res["result"] = $hasResult["result"];

        } else {

            $this->res["success"] = false;
            $this->res["result"] = $hasResult["result"];

        }

        return $this->res;

    }

    public function countApplicants() {

        $this->createConn();

        $this->query("SELECT count(app_id) as count FROM applicant WHERE app_status = 'Pending' ");

        $hasResult = $this->resultSet();

        if ($hasResult["success"] == true) {

            $this->res["success"] = true;
            $this->res["result"] = $hasResult["result"];

        } else {

            $this->res["success"] = false;
            $this->res["result"] = "0";

        }

        return $this->res;

    }    

}