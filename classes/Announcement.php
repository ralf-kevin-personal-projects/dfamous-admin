<?php

class Announcement extends Database {

    
    private $res;

    public function __construct($args, $req_code) {
        switch ($req_code) {
            case "fetch_ann":
            break;
            default:
            break;
        }
    }




    public function fetchAll() {

        $this->createConn();

        $this->query("SELECT * FROM announcement ORDER BY ann_id DESC ");

        $hasResult = $this->resultSet();

        if ($hasResult["success"] == true) {

            $this->res["success"] = true;
            $this->res["result"] = $hasResult["result"];

        } else {

            $this->res["success"] = false;
            $this->res["result"] = $hasResult["result"];

        }

        return $this->res;

    }


    public function countApplicants() {

        $this->createConn();

        $this->query("SELECT count(app_id) as count FROM applicant WHERE app_status = 'Pending' ");

        $hasResult = $this->resultSet();

        if ($hasResult["success"] == true) {

            $this->res["success"] = true;
            $this->res["result"] = $hasResult["result"];

        } else {

            $this->res["success"] = false;
            $this->res["result"] = "0";

        }

        return $this->res;

    }    

}