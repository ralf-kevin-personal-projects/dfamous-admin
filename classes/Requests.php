<?php

require('../config.php');
require('Database.php');
require('Validation.php');
require('Auth.php');
require('Company.php');
require('Applicant.php');
require('Employee.php');
require('Announcement.php');
require('Requirements.php');
require('Reports.php');


$json_response = array();
$validation = new Validation();

if (isset($_POST["request"])) {
    $req = $_POST["request"];

    $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);
    
    if ($validation->checkItems($_POST)) {
        
        switch ($req) {
            case "login":
                $auth = new Auth($_POST, "login");
                $json_response = $auth->login();
            break;
            case "forgot_pass":
                $auth = new Auth($_POST, "forgot_pass");
                $json_response = $auth->forgotPass();
            break;

            // Company
            case "create_company":
                $company = new Company($_POST, "create_company");
                $json_response = $company->createAccount();
            break;
            case "edit_company":
                $company = new Company($_POST, "edit_company");
                $json_response = $company->updateAccount();
            break;            
            case "fetch_company":
                $company = new Company($_POST, "fetch_company");
                $json_response = $company->fetchAll();          
            break;

            // Company Requests
            case "fetch_request":
                $company = new Company($_POST, "fetch_request");
                $json_response = $company->fetchRequest();
            break;
            case "request_override":
                $company = new Company($_POST, "request_override");
                $json_response = $company->override();
            break; 
            case "count_requests":
                $company = new Company($_POST, "count_requests");
                $json_response = $company->countRequest();
            break;                        
            

            // Applicants
            case "fetch_applicant":
                $applicant = new Applicant($_POST, "fetch_applicant");
                $json_response = $applicant->fetchAll();          
            break;
            case "app_override":
                $applicant = new Applicant($_POST, "app_override");
                $json_response = $applicant->override();
            break;
            case "count_applicants":
                $applicant = new Applicant($_POST, "count_applicants");
                $json_response = $applicant->countApplicants();
            break;            


            // Employee
            case "create_emp":
                $employee = new Employee($_POST, "create_emp");
                $json_response = $employee->createAccount();          
            break;
            case "fetch_accepted_app":
                $employee = new Employee($_POST, "fetch_accepted_app");
                $json_response = $employee->fetchApplicant();          
            break;
            case "fetch_emp":
                $employee = new Employee($_POST, "fetch_emp");
                $json_response = $employee->fetchEmployee();          
            break;            


            // Announcement
            case "fetch_ann":
                $announcement = new Announcement($_POST, "fetch_ann");
                $json_response = $announcement->fetchAll();          
            break;


            //Requirements
            case "fetch_admin":
                $requirements = new Requirements($_POST, "fetch_admin");
                $json_response = $requirements->fetchAdminSide();          
            break;
            case "fetch_company_list":
                $requirements = new Requirements($_POST, "fetch_company_list");
                $json_response = $requirements->fetchCompanyList();
            break;            
            case "fetch_company_req":
                $requirements = new Requirements($_POST, "fetch_company_req");
                $json_response = $requirements->fetchCompanySide();
            break;


            //Reports
            case "fetch_emp_list":
                $reports = new Reports($_POST, "fetch_emp_list");
                $json_response = $reports->fetchEmpList();          
            break;
            case "fetch_app_list":
                $reports = new Reports($_POST, "fetch_app_list");
                $json_response = $reports->fetchAppList();
            break;            
            case "fetch_comp_list":
                $reports = new Reports($_POST, "fetch_comp_list");
                $json_response = $reports->fetchCompList();
            break;



            default:
                $json_response["success"] = false;
                $json_response["result"] = "Unknown Request";
            break;
        }
        
    } else {
        $json_response["success"] = false;
        $json_response["result"] = "Empty Post Value";        
    }

} else {
    $json_response["success"] = false;
    $json_response["result"] = "Empty Request Value";
}

echo json_encode($json_response);