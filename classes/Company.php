<?php

class Company extends Database {

    private $id;


    private $name;
    private $address;
    private $loc;
    private $desc;
    private $email;
    private $num;
    private $user;
    private $pass;

    private $hashPass;
    
    private $request_id;
    private $status;

    private $res;

    public function __construct($args, $req_code) {
        switch ($req_code) {
            case "create_company":
                $this->name = $args["name"];
                $this->address = $args["address"];
                $this->loc = $args["loc"];
                $this->desc = $args["desc"];
                $this->email = $args["email"];
                $this->num = $args["num"];
                $this->user = $args["user"];
                $this->pass = $args["pass"];
            break;
            case "edit_company":
                $this->name = $args["name"];
                $this->address = $args["address"];
                $this->loc = $args["loc"];
                $this->desc = $args["desc"];
                $this->email = $args["email"];
                $this->num = $args["num"];
                $this->user = $args["user"];
                $this->pass = $args["pass"];

                $this->status = $args["status"];
                $this->id = $args["id"];
            break;
            case "fetch_company":
            break;
            case "request_override":
                $this->request_id = $args["request_id"];
                $this->status = $args["status"];
            break;
            default:

            break;
        }
    }


    public function createAccount() {
        
        $this->createConn();

        $this->hashPass = md5($this->pass);

        $this->query("INSERT INTO acct_company 
                    (company_user, company_pass, company_name, company_address, company_location, company_desc, company_email, 
                    company_contact)
                    VALUES
                    ( '". $this->user ."', '". $this->hashPass ."', '". $this->name ."', '". $this->address ."',
                    '". $this->loc ."', '". $this->desc ."', '". $this->email ."', '". $this->num ."' ) ");

        $hasResult = $this->insertData();

        if ($hasResult["success"] == true) {

            $this->res["success"] = true;
            $this->res["result"] = $hasResult["result"];

        } else {

            $this->res["success"] = false;
            $this->res["result"] = $hasResult["result"];

        }
        
        return $this->res;
    }


    public function updateAccount() {

        $this->createConn();

        $this->hashPass = md5($this->pass);

        $this->query("UPDATE acct_company 
                    SET 
                    company_user = '". $this->user ."', company_pass = '". $this->hashPass ."', 
                    company_name = '". $this->name ."', company_address = '". $this->address ."', 
                    company_location = '". $this->loc ."', company_desc = '". $this->desc ."', 
                    company_email = '". $this->email ."', company_contact = '". $this->num ."', 
                    status = '". $this->status ."'
                    
                    WHERE company_id = '". $this->id ."' ");

        $hasResult = $this->updateData();

        if ($hasResult["success"] == true) {

            $this->res["success"] = true;
            $this->res["result"] = $hasResult["result"];

        } else {

            $this->res["success"] = false;
            $this->res["result"] = $hasResult["result"];

        }

        return $this->res;

    }


    public function override() {
       
        $this->createConn();

        $this->query("UPDATE company_request SET status = '". $this->status ."'
                    WHERE request_id = '". $this->request_id ."' ");

        $hasResult = $this->updateData();

        if ($hasResult["success"] == true) {

            $this->res["success"] = true;
            $this->res["result"] = $hasResult["result"];

        } else {

            $this->res["success"] = false;
            $this->res["result"] = $hasResult["result"];

        }

        return $this->res;

    }


    public function fetchAll() {

        $this->createConn();

        $this->query("SELECT 
                    company_id, company_user, company_name, company_address, company_location, company_desc, 
                    company_email, company_contact, status, date_created 
                    FROM acct_company 
                    ORDER BY company_id DESC ");

        $hasResult = $this->resultSet();

        if ($hasResult["success"] == true) {

            $this->res["success"] = true;
            $this->res["result"] = $hasResult["result"];

        } else {

            $this->res["success"] = false;
            $this->res["result"] = $hasResult["result"];

        }

        return $this->res;

    }



    public function fetchRequest() {

        $this->createConn();

        $this->query("SELECT 
                    company.company_id, company.company_name, req.request_id, req.request_type, req.request_desc, req.status, req.date_created
                    FROM acct_company as company
                    INNER JOIN company_request as req
                    ON company.company_id = req.company_id
                    ORDER BY req.request_id DESC ");

        $hasResult = $this->resultSet();

        if ($hasResult["success"] == true) {

            $this->res["success"] = true;
            $this->res["result"] = $hasResult["result"];

        } else {

            $this->res["success"] = false;
            $this->res["result"] = $hasResult["result"];

        }

        return $this->res;

    }


    public function countRequest() {

        $this->createConn();

        $this->query("SELECT count(request_id) as count FROM company_request WHERE status = 'Pending' ");

        $hasResult = $this->resultSet();

        if ($hasResult["success"] == true) {

            $this->res["success"] = true;
            $this->res["result"] = $hasResult["result"];

        } else {

            $this->res["success"] = false;
            $this->res["result"] = "0";

        }

        return $this->res;

    }

}