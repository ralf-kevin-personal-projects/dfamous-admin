<?php

class Reports extends Database {

    private $company_id;

    private $res;

    public function __construct($args, $req_code) {
        switch ($req_code) {
            case "fetch_emp_list":
            break;
            case "fetch_app_list":
            break;
            case "fetch_comp_list":
            break;            
            default:

            break;
        }
    }




    public function fetchEmpList() {

        $this->createConn();

        $this->query("SELECT * FROM acct_emp ORDER BY emp_id DESC ");

        $hasResult = $this->resultSet();

        if ($hasResult["success"] == true) {

            $this->res["success"] = true;
            $this->res["result"] = $hasResult["result"];

        } else {

            $this->res["success"] = false;
            $this->res["result"] = $hasResult["result"];

        }

        return $this->res;

    }


    public function fetchAppList() {

        $this->createConn();

        $this->query("SELECT * FROM applicant as app 
                    INNER JOIN acct_company as comp
                    ON app.company_id = comp.company_id
                    ORDER BY app_id DESC ");

        $hasResult = $this->resultSet();

        if ($hasResult["success"] == true) {

            $this->res["success"] = true;
            $this->res["result"] = $hasResult["result"];

        } else {

            $this->res["success"] = false;
            $this->res["result"] = $hasResult["result"];

        }

        return $this->res;

    }

    public function fetchCompList() {

        $this->createConn();

        $this->query("SELECT * FROM acct_company ORDER BY company_id DESC ");

        $hasResult = $this->resultSet();

        if ($hasResult["success"] == true) {

            $this->res["success"] = true;
            $this->res["result"] = $hasResult["result"];

        } else {

            $this->res["success"] = false;
            $this->res["result"] = $hasResult["result"];

        }

        return $this->res;

    }
 

}