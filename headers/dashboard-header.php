<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Dfamous - Admin</title>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link rel="stylesheet" type="text/css" media="screen" href="../assets/css/bootstrap/bootstrap.min.css" />
    <link rel="stylesheet" type="text/css" media="screen" href="../assets/css/main.css" />
    <link rel="stylesheet" type="text/css" media="screen" href="../assets/plugins/fontawesome/css/all.css" />

    <script src="../assets/js/jquery-3.2.1.min.js"></script>
    <script src="../assets/js/bootstrap/bootstrap.min.js"></script>
</head>
<body>
    <nav class="navbar navbar-dark fixed-top bg-green flex-md-nowrap p-0 shadow">
        <a class="navbar-brand col-sm-3 col-md-2 mr-0" href="#">D'FAMOUS MANPOWER</a>
        <ul class="navbar-nav px-3">
            <li class="nav-item text-nowrap">
            <a id="log-out" class="nav-link" >Welcome, Sign out</a>
            </li>
        </ul>
    </nav>
    
    <div class="container-fluid">
        <div class="row">

            <nav class="col-md-2 d-none d-md-block bg-light sidebar">
                <div class="sidebar-sticky">
                  <ul class="nav flex-column">
                    <li class="nav-item">
                        <a class="nav-link" href="company.php">
                            <i class="fas fa-home"></i>
                            Company Client
                        </a>
                        <!-- <ul>
                            <li class="sub-menu"><i class="fas fa-plus"></i> Add Company</li>
                        </ul> -->

                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="company-request.php">
                            <i class="fas fa-bell"></i>
                            Company Requests
                            <span id="request-notif" class="notif">999</span>
                        </a>
                        <!-- <ul>
                            <li class="sub-menu"><i class="fas fa-plus"></i> Add Company</li>
                        </ul> -->

                    </li>                    

                    <li class="nav-item">
                        <a class="nav-link" href="employee.php">
                            <i class="fas fa-users"></i>
                            Employee
                        </a>
                        <!-- <ul>
                            <li class="sub-menu">
                                <a class="nav-link-sub" href="#">
                                    <i class="fas fa-folder"></i> Requirements
                                </a>
                            </li>
                        </ul> -->
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="announcement.php">
                            <i class="fas fa-bell"></i>
                             Anouncement
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="requirements.php">
                            <i class="fas fa-list-alt"></i>                            
                            Requirements
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="applicant.php">
                            <i class="fas fa-chart-pie"></i>
                            Applicants
                            <span id="applicant-notif" class="notif">999</span>                            
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="reports.php">
                            <i class="fas fa-folder"></i>
                            Reports
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="setting.php">
                            <i class="fas fa-cog"></i>
                            Password Setting
                        </a>
                    </li>                    
                  </ul>

                </div>
            </nav>