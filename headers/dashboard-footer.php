
        </div>
    </div>

</body>
</html>

<script>
    $(document).ready(function() {

        checkSessions();
        loadRequestNotif();
        loadApplicanttNotif();


        $("#log-out").click(function() {

            if(confirm("Are you sure you want to Logout?")){
                window.localStorage.removeItem("admin_id");
                window.localStorage.removeItem("admin_role");
                window.localStorage.removeItem("admin_status");
                window.localStorage.removeItem("admin_user");            
                // window.localStorage.clear();
                location.href = "../";
            }

        });

        function checkSessions() {
            if (typeof(Storage) !== "undefined") {

                if (localStorage.length < 0 || localStorage.length == 0) {

                    location.href = "../";

                } else {

                    if (localStorage.getItem("admin_user") == null || localStorage.getItem("admin_user") == "") {

                        location.href = "../";

                    } else {
                        var name = "Welcome " + localStorage.getItem("admin_user") + ", Sign Out";
                        $("#log-out").text(name)

                    }
                }

            } else {

                alert("Your Browser is not supported to our function, please download better browser")
                location.href = "../";

            }
        }

        function loadRequestNotif() {

            var fd = new FormData();
            fd.append("request", "count_requests");

            $.ajax({
                type: "POST",
                url: "../classes/Requests.php",
                data: fd,
                contentType: false,
                cache: false,
                processData: false,                  
                dataType: "json",
                success: function(res){
                    $("#request-notif").text(res.result[0]["count"]);
                }, error: function() {
                    alert("error handler")
                }
            });

        }


        function loadApplicanttNotif() {

            var fd = new FormData();
            fd.append("request", "count_applicants");

            $.ajax({
                type: "POST",
                url: "../classes/Requests.php",
                data: fd,
                contentType: false,
                cache: false,
                processData: false,                  
                dataType: "json",
                success: function(res){
                    $("#applicant-notif").text(res.result[0]["count"]);
                }, error: function() {
                    alert("error handler")
                }
            });

        }        

    });


</script>